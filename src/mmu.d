import cart;
import dbg;
import type;
import bindbc.sdl;
import std.format, 
       std.stdio;

//Addresses of memory mapped IO registers
enum ExtraReg : ushort {
    JoyPad     = 0xFF00, TimerDiv   = 0xFF04, TimerCount = 0xFF05,
    TimerMod   = 0xFF06, TimerCon   = 0xFF07, IFLAGS     = 0xFF0F,
    AC1Sweep   = 0xFF10, AC1Duty    = 0xFF11, AC1Vol     = 0xFF12,
    AC1LData   = 0xFF13, AC1HData   = 0xFF14, AC2Duty    = 0xFF16,
    AC2Vol     = 0xFF17, AC2LData   = 0xFF18, AC2HData   = 0xFF19, 
    AC3Enable  = 0xFF1A, AC3Vol     = 0xFF1C, AC3LData   = 0xFF1D, 
    AC3HData   = 0xFF1E, AC4Vol     = 0xFF21, AC4Freq    = 0xFF22,
    AudioVol   = 0xFF24, AudioSel   = 0xFF25, AudioSet   = 0xFF26,
    AC3Wave    = 0xFF30, LCDCONT    = 0xFF40, LCDStat    = 0xFF41,
    yScroll    = 0xFF42, xScroll    = 0xFF43, curScan    = 0xFF44,
    scanComp   = 0xFF45, DMA        = 0xFF46, BGPalette  = 0xFF47,
    OBPalette0 = 0xFF48, OBPalette1 = 0xFF49, winYPos    = 0xFF4A,
    winXPos    = 0xFF4B, GBCSpeedx2 = 0xFF4D, GBCVRam    = 0xFF4F, 
    GBCBPIndex = 0xFF68, GBCBP      = 0xFF69, GBCRam     = 0xFF70, 
    IEFlags    = 0xFFFF
}

//Type used to represent a memory management unit, containing memory and registers usable by both CPU and GPU
class MMU {
    private:
        Cartridge cart;
        ubyte[4*1024][] internalRAM;
        ubyte[8*1024][] vRAM;
        ubyte[0x7F  ] upperRAM;
        ubyte[0xA0  ] spriteAttrib;
        ubyte[0x80  ] registerBuf;
        ubyte IEReg; 

        GBType type;
        ubyte[2][0x3F] mGBCPalette;
        ubyte gbcRamSelector;
        ubyte gbcVRamSelector;
     
        size_t divTime;
        size_t countTime;
        bool[4] directions;
        bool[4] buttons;

        bool addressUsable(ushort address) {
            //Return false if an address of an unusable cartridge RAM region or empty memory region is passed
            return !( ((address >= 0xA000 && address < 0xC000) && !cart.ramBankUsable(address)) ||
                       (address >= 0xFEA0 && address < 0xFF00) ||
                       (address >= 0xFF4C && address < 0xFF80) );
        }

    final public:
        bool debugMode;

        ref T rawRead(T)(ushort address, int customVRAMSelect = -1) {
            if (address < 0x8000) {
                //Cartridge ROM banks
                return cart.rawRead!T(address);
            } else if (address < 0xA000) {
                //Video RAM
                if (type == GBType.GB) {
                    return *cast(T*)(&vRAM[0][address-0x8000]);
                } else if (type == GBType.GBC) {
                    return *cast(T*)(&vRAM[customVRAMSelect > 0 ? customVRAMSelect : gbcVRamSelector][address-0x8000]);
                }
                assert(false);
            } else if (address < 0xC000) {
                //Cartridge RAM bank
                return cart.rawRead!T(address);
            } else if (address < 0xFE00) {
                //Main internal RAM
                auto offset = (address-0xC000)%0x2000;
                if (type == GBType.GB) {
                    return *cast(T*)(&internalRAM[offset/0x1000][offset%0x1000]);
                } else if (type == GBType.GBC) {
                    if (offset < 0x1000) {
                        return *cast(T*)(&internalRAM[0][offset]);
                    } else {
                        return *cast(T*)(&internalRAM[gbcRamSelector][offset-0x1000]);
                    }
                }
                assert(false);
            } else if (address < 0xFEA0) {
                //Sprite attribute memory (OAM)
                return *cast(T*)(&spriteAttrib[address-0xFE00]);
            } else if (address < 0xFF00) {
                //Empty but unusable for I/O
                throw new Exception(format("Unusable address 0x%04x accessed directly", address));
            } else if (address < 0xFF80) {
                //IO registers
                return *cast(T*)(&registerBuf[address-0xFF00]);
            } else if (address < 0xFFFF) {
                //Upper internal RAM
                return *cast(T*)(&upperRAM[address - 0xFF80]);
            } else if (address == 0xFFFF) {
                //Interrupt enable register
                return *cast(T*)&IEReg;
            } else {
                throw new Exception(format("Unimplemented memory address 0x%04x", address));
            }
        }

        T read(T)(ushort address, int customVRAMSelect = -1) {
            T toReturn;

            if (!addressUsable(address)) {
                (cast(ubyte*)&toReturn)[0..T.sizeof] = 0;
                return toReturn;
            }

            toReturn = rawRead!T(address, customVRAMSelect);
            if (debugMode) {
                writefln("MMU : Read \"%(%02x %)\" (type %s) from address %04x", cast(ubyte[])((&toReturn)[0..1]), T.stringof, address);
            }
            return toReturn;
        }

        void write(T)(ushort address, T value) { 
            if (debugMode) {
                writefln("MMU : Write \"%(%02x %)\" (type %s) to address %04x", cast(ubyte[])((&value)[0..1]), T.stringof, address);
            }    

            if (address < 0x8000 && addressUsable(address)) {
                //Cartridge ROM banks
                cart.write!T(address, value);
                return;
            }
            switch (address) with (ExtraReg) {
                case TimerDiv:
                    //Reset the timer divider register if it is written to
                    rawRead!ubyte(address) = 0;
                    return;
                case JoyPad:
                    //Select button or direction keys for the joypad register
                    rawRead!ubyte(JoyPad) = value & 0x30;
                    updateJoypad;
                    return;
                case DMA:
                    //Initiate a DMA transfer of memory to OAM memory region
                    foreach (i,e; read!(ubyte[0x8C])(cast(ushort)(cast(ushort)(value) << 8))) {
                        write(cast(ushort)(0xFE00+i), e);
                    }
                    return;
                case GBCSpeedx2:
                    //Prepare speed switch
                    rawRead!ubyte(GBCSpeedx2) = (rawRead!ubyte(GBCSpeedx2) & 0xFE) | (value & 0x1);
                    return;
                case GBCVRam:
                    //Select vRAM bank for gameboy color
                    gbcVRamSelector = value & 0x1;
                    rawRead!ubyte(GBCVRam) = value & 0x1;
                    return;
                case GBCRam:
                    //Select internal RAM bank for gameboy color
                    gbcRamSelector = value & 0x7;
                    if (!gbcRamSelector) {
                        gbcRamSelector = 1;
                    }
                    return;
                default:
                    if (addressUsable(address)) {
                        rawRead!T(address) = value;
                    }
            }
        }

        void updateJoypad() {
            auto select = (rawRead!ubyte(ExtraReg.JoyPad) & 0x30) >> 4;
            if (select == 0b10) {
                //Update based on direction keys
                ubyte flags;
                foreach_reverse (i; directions) {
                    flags <<= 1;
                    flags |= !i;
                }
                rawRead!ubyte(ExtraReg.JoyPad) = 0x20 | flags;
            } else if (select == 0b01) {
                //Update based on button keys
                ubyte flags;
                foreach_reverse (i; buttons) {
                    flags <<= 1;
                    flags |= !i;
                }
                rawRead!ubyte(ExtraReg.JoyPad) = 0x10 | flags;
            } else {
                rawRead!ubyte(ExtraReg.JoyPad) = 0x3F;
            }
        }

        void setKey(SDL_Keycode code, bool pressed = true) {
            switch (code) {
                case SDLK_w: directions[2] = pressed; break;
                case SDLK_a: directions[1] = pressed; break;
                case SDLK_s: directions[3] = pressed; break;
                case SDLK_d: directions[0] = pressed; break;
                case SDLK_q: buttons   [3] = pressed; break;
                case SDLK_e: buttons   [2] = pressed; break;
                case SDLK_z: buttons   [0] = pressed; break;
                case SDLK_x: buttons   [1] = pressed; break;
                default: return;
            }
            if (pressed) {
                write!ubyte(ExtraReg.IFLAGS, read!ubyte(ExtraReg.IFLAGS) | 0x10);
            }
            updateJoypad;
        }

        void updateTimer(ulong time) {
            //Update cartridge RTC
            cart.updateRTC(time);

            //Update the divider register by incrementing it at a rate of 16384Hz
            rawRead!ubyte(ExtraReg.TimerDiv) = cast(ubyte)( read!ubyte(ExtraReg.TimerDiv)+(time-divTime > 1e9/16384.0) );

            //Update the timer counter if it is enabled
            ubyte timerCon = read!ubyte(ExtraReg.TimerCon);
            if (timerCon & 0x4) {
                //Determine the update speed
                size_t updateRate;
                final switch (timerCon & 0x3) {
                    case 0b00:
                        updateRate = 4096;
                        break;
                    case 0b01:
                        updateRate = 262144;
                        break;
                    case 0b10:
                        updateRate = 65536;
                        break;
                    case 0b11:
                        updateRate = 16384;
                        break;
                }

                if (time-countTime > 1e9/updateRate) {
                    //If the timer counter overflows then set it to the value in the timer modulo register and cause an interrupt
                    uint result = read!ubyte(ExtraReg.TimerCount)+1;
                    if (result > 0xFF) {
                        write(ExtraReg.TimerCount, read!ubyte(ExtraReg.TimerMod));
                        write!ubyte(ExtraReg.IFLAGS, read!ubyte(ExtraReg.IFLAGS) | 0x4);
                    } else {
                        write(ExtraReg.TimerCount, cast(ubyte)result);
                    }

                    countTime = time;
                }
            }

            if (time-divTime > 1e9/16384.0) {
                divTime = time;
            }
        }

        void reset(GBType type, bool dbg = false) {
            this.type = type;
            debugMode = dbg;

            if (type == GBType.GB) {
                //There is a singular 8KiB unit of internal RAM on the original gameboy, as well as 8KiB of vRAM
                internalRAM.length = 2;
                vRAM.length = 1;
            } else if (type == GBType.GBC) {
                //There is 32KiB of internal RAM split into 4KiB banks on the gameboy color, as well as two banks of 8KiB for vRAM
                internalRAM.length = 8;
                vRAM.length = 2;
                gbcRamSelector = 1;
            }
        }

        ubyte[2][] gbcPalette() {
            return cast(ubyte[2][])(mGBCPalette[]);
        }

        void loadSave() {
            cart.loadSave;
        }

        void storeSave() {
            cart.storeSave;
        }

        void rom(string file) {
            cart = Cartridge(file);
        }

        this(string file) {
            rom = file;
        }
}
