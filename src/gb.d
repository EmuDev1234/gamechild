import cpu;
import gpu;
import apu;
import mmu;
import dbg;
public import type;
import std.stdio;
import bindbc.sdl;
    
struct GB {
    private:
        CPU cpu;
        GPU gpu;
        APU apu;
        MMU mmu;
        GBType type;
        ulong timer;
        bool mPower = false;
    
    public:
        bool power() {
            return mPower;
        }

        MMU memory() {
            return mmu;
        }

        void reset(Debug dbg = Debug.NONE) {
            //Configure the CPU, GPU and APU
            cpu.reset(type, cast(bool)(dbg & Debug.CPU), mmu);
            gpu.reset(type, cast(bool)(dbg & Debug.GPU), mmu);
            apu.reset(type, cast(bool)(dbg & Debug.APU), mmu);
            mmu.reset(type, cast(bool)(dbg & Debug.MMU)); 
            mPower = true;
        }

        bool update() {
            if (!mPower) {
                return false;
            } else {
                //Emulate an entire frame in one run
                ulong time = timer;
                while (time < timer+(1e9/59.7)) {
                    gpu.update(time);
                    apu.update(time);
                    time += cpu.update(time);
                }
                timer = time;
                return true;
            }
        }

        void keyEvent(SDL_Keycode key, bool press = true) {
            if (press) {
                mmu.setKey(key);
                if (key == SDLK_p) {
                    cpu.halt;
                }
            } else {
                mmu.setKey(key, false);
            }
        }
        
        void loadGame(string file, Debug dbg = Debug.NONE, GBType type = GBType.Auto) {
            mmu = new MMU(file);
            if (type == GBType.Auto) {
                //Automatically emulate a Gameboy Color if the game is designed for it
                auto cartType = mmu.read!ubyte(0x143);
                if (cartType == 0x80 || cartType == 0xC0) {
                    this.type = GBType.GBC;
                } else {
                    this.type = GBType.GB;
                }
                writeln("Automatically selected ", this.type, " mode");
            } else {
                this.type = type;
            }
            reset(dbg);
        }

        void closeGame() {
            mmu.storeSave;
            mPower = false;
        }
}
