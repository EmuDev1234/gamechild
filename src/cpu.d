import mmu;
import dbg;
import type;
import std.string, 
       std.format,
       std.stdio,
       std.conv;

struct CPU {
    private:
        //CPU registers
        byte[8] GPReg;
        ushort PC;
        ushort SP;
        enum GPRegister {
            A, B, C, D, E, F, H, L
        }
        enum FFlag {
            Z = 0x80, N = 0x40, H = 0x20, C = 0x10
        }

        //Unrelated variables
        MMU mmu;

        bool halted;
        bool interruptsEnabled;
        bool disableInterrupts;
        bool enableInterrupts;
    
        bool debugMode;
        bool debugOutput;
        ulong clockSpeed = 4194304;
        GBType type;
        ushort cycleTimer;
        size_t instCount;
        ushort breakPC;
        int breakOP;
        string prevCmd;

        //Helper functions
        void testInterrupt(ubyte flag, ushort isr) {
            if (!interruptsEnabled) {
                return;
            }

            ubyte IFLAGS = mmu.read!ubyte(ExtraReg.IFLAGS);
            if ((IFLAGS & flag) && (mmu.read!ubyte(ExtraReg.IEFlags) & flag)) {
                interruptsEnabled = false;
                CALL(isr);
                mmu.write!ubyte(ExtraReg.IFLAGS, IFLAGS & ~flag);
            }
        }

        void unimplementedInstruction(ubyte op, ubyte op2 = 0) {
            string msg;
            if (op == 0xCB) {
                msg = format("Unknown opcode \"%02x %02x\" at 0x%04x", op, op2, PC-1);
            } else {
                msg = format("Unknown opcode \"%02x\" at 0x%04x", op, PC-1);
            }
            throw new Exception(msg);
        }

        ushort doubleReg(GPRegister A, GPRegister B) {
            return cast(ushort)((ushort(GPReg[A]) << 8) | (ushort(GPReg[B]) & 0xFF));
        }

        void setFlag(FFlag flag, bool value) {
            GPReg[GPRegister.F] &= ~flag;
            if (value) {
                GPReg[GPRegister.F] |= flag;
            }
        }

        void clearFlags() {
            foreach (i; [FFlag.Z, FFlag.N, FFlag.H, FFlag.C]) {
                setFlag(i, false);
            }
        }

        //Instruction functions
        void LD(GPRegister reg1, GPRegister reg2, GPRegister src) {
            mmu.write(doubleReg(reg1, reg2), GPReg[src]);
        }

        void LD(GPRegister reg1, GPRegister reg2) {
            auto value = mmu.read!ushort(PC);
            GPReg[reg1] = cast(byte)(value >> 8);
            GPReg[reg2] = cast(byte)value;
            PC += 2;
        }

        void LD(GPRegister register) {
            GPReg[register] = mmu.read!byte(PC++);
        }

        void LDHL() {
            byte add = mmu.read!byte(PC++);
            ushort HL = cast(ushort)(SP + add);
            GPReg[GPRegister.H] = cast(byte)(HL >> 8);
            GPReg[GPRegister.L] = cast(byte)HL;

            clearFlags;
            setFlag(FFlag.H, cast(bool)(((SP & 0x0F) + (add & 0x0F)) > 0x0F));
            setFlag(FFlag.C, cast(bool)(((SP ^ add ^ (SP+add & 0xFFFF)) & 0x100)));
        }

        void INC(ref byte value) {
            setFlag(FFlag.H, (value & 0xF)+1 > 0xF);
            ++value;
            setFlag(FFlag.Z, !value);
            setFlag(FFlag.N, false); 
        }

        void INC(ushort address) {
            byte value = mmu.read!byte(address);
            INC(value);
            mmu.write(address, value);
        }

        void INC(GPRegister reg1, GPRegister reg2) {
            ushort reg = doubleReg(reg1, reg2);
            ++reg;
            GPReg[reg1] = cast(byte)(reg >> 8);
            GPReg[reg2] = cast(byte)reg;
        }

        void DEC(ref byte value) {
            setFlag(FFlag.H, (value & 0xF)-1 < 0);
            --value;
            setFlag(FFlag.Z, !value);
            setFlag(FFlag.N, true);   
        }

        void DEC(ushort address) {
            byte value = mmu.read!byte(address);
            DEC(value);
            mmu.write(address, value);
        }

        void DEC(GPRegister reg1, GPRegister reg2) {
            ushort reg = doubleReg(reg1, reg2);
            --reg;
            GPReg[reg1] = cast(byte)(reg >> 8);
            GPReg[reg2] = cast(byte)reg;
        }

        void ADDSP() {
            clearFlags;
            byte value = mmu.read!byte(PC++);
            setFlag(FFlag.H, ((SP ^ value ^ (SP+value & 0xFFFF)) & 0x10 ) == 0x10);
            setFlag(FFlag.C, ((SP ^ value ^ (SP+value & 0xFFFF)) & 0x100) == 0x100);
            SP += value;
        }

        void ADD(byte value) {
            setFlag(FFlag.C, cast(bool)((ubyte(GPReg[GPRegister.A]) + ubyte(value)) & 0x100));
            setFlag(FFlag.H, (GPReg[GPRegister.A] & 0xF) + (value & 0xF) > 0xF);
            GPReg[GPRegister.A] += value;
            setFlag(FFlag.Z, !GPReg[GPRegister.A]);
            setFlag(FFlag.N, false);
        }

        void ADDC(byte value) {
            ushort toAdd = ubyte(value) + cast(bool)(GPReg[GPRegister.F] & FFlag.C);

            setFlag(FFlag.H, (GPReg[GPRegister.A] & 0xF) + ((value & 0xF) + cast(bool)(GPReg[GPRegister.F] & FFlag.C)) > 0xF);
            setFlag(FFlag.C, cast(bool)((ubyte(GPReg[GPRegister.A]) + toAdd) & 0x100));
            GPReg[GPRegister.A] += toAdd;
            setFlag(FFlag.Z, !GPReg[GPRegister.A]);
            setFlag(FFlag.N, false);
        }

        void ADD(ushort value) {
            ushort reg = doubleReg(GPRegister.H, GPRegister.L);
            setFlag(FFlag.N, false);
            setFlag(FFlag.H, (reg & 0xFFF) + (value & 0xFFF) > 0xFFF);
            setFlag(FFlag.C, cast(bool)((reg+value) & 0x10000));

            reg += value;
            GPReg[GPRegister.H] = cast(byte)(reg >> 8);
            GPReg[GPRegister.L] = cast(byte)reg;
        }
        
        void SUB(byte value) {
            setFlag(FFlag.C, ubyte(GPReg[GPRegister.A]) < ubyte(value));
            setFlag(FFlag.H, (GPReg[GPRegister.A] & 0xF) < (value & 0xF));
            GPReg[GPRegister.A] -= value;
            setFlag(FFlag.Z, !GPReg[GPRegister.A]);
            setFlag(FFlag.N, true);
        }

        void SUBC(byte value) {
            ushort toSub = ubyte(value) + cast(bool)(GPReg[GPRegister.F] & FFlag.C);

            setFlag(FFlag.H, (GPReg[GPRegister.A] & 0xF) < ((value & 0xF) + cast(bool)(GPReg[GPRegister.F] & FFlag.C)));
            setFlag(FFlag.C, ubyte(GPReg[GPRegister.A]) < ushort(toSub));
            GPReg[GPRegister.A] -= toSub;
            setFlag(FFlag.Z, !GPReg[GPRegister.A]);
            setFlag(FFlag.N, true);
        }

        void AND(byte value) {
            GPReg[GPRegister.A] &= value;
            clearFlags;
            setFlag(FFlag.Z, !GPReg[GPRegister.A]);
            setFlag(FFlag.H, true);
        }

        void OR(byte value) {
            GPReg[GPRegister.A] |= value;
            clearFlags;
            setFlag(FFlag.Z, !GPReg[GPRegister.A]);
        }

        void XOR(byte value) {
            GPReg[GPRegister.A] ^= value;
            clearFlags;
            setFlag(FFlag.Z, !GPReg[GPRegister.A]);
        }

        void CCF() {
            setFlag(FFlag.N, false); 
            setFlag(FFlag.H, false); 
            setFlag(FFlag.C, !(GPReg[GPRegister.F] & FFlag.C));
        }

        void SCF() {
            setFlag(FFlag.N, false); 
            setFlag(FFlag.H, false); 
            setFlag(FFlag.C, true );
        }

        void SWAP(ref byte value) {
            value = cast(byte)(( (value >> 4) & 0xF ) | (value << 4));
            clearFlags;
            setFlag(FFlag.Z, !value);
        }

        void SWAP(ushort address) {
            byte value = mmu.read!byte(address);
            SWAP(value);
            mmu.write(address, value);
        }
        
        void POP(GPRegister reg1, GPRegister reg2) {
            //Pop a value from the stack into a double register - make sure the lowest 4 bits of F always go unused!
            ushort value = mmu.read!ushort(SP);
            SP += 2;
            GPReg[reg1] = cast(byte)(value >> 8);
            GPReg[reg2] = reg2 == GPRegister.F ? cast(byte)(value & 0xF0) : cast(byte)value;
        }   

        void PUSH(GPRegister reg1, GPRegister reg2) {
            SP -= 2;                    
            mmu.write(SP, doubleReg(reg1, reg2));
        }

        void DAA() {
            //Adjust the value of the A register to convert it to BCD format
            if (!(GPReg[GPRegister.F] & FFlag.N)) {
                //If the last operation was an addition
                if ((GPReg[GPRegister.F] & FFlag.C) || (ubyte(GPReg[GPRegister.A]) > 0x99)) {
                    GPReg[GPRegister.A] += 0x60;
                    setFlag(FFlag.C, true);
                }
                if ((GPReg[GPRegister.F] & FFlag.H) || (ubyte(GPReg[GPRegister.A] & 0x0F) > 0x09)) {
                    GPReg[GPRegister.A] += 0x6;
                }
            } else {
                //If the last operation was a subtraction
                if (GPReg[GPRegister.F] & FFlag.C) {
                    GPReg[GPRegister.A] -= 0x60;
                    setFlag(FFlag.C, true);
                }
                if (GPReg[GPRegister.F] & FFlag.H) {
                    GPReg[GPRegister.A] -= 0x6;
                }
            }

            setFlag(FFlag.Z, !GPReg[GPRegister.A]);
            setFlag(FFlag.H, false);
        }

        void CPL() {
            GPReg[GPRegister.A] = ~GPReg[GPRegister.A];
            setFlag(FFlag.N, true);
            setFlag(FFlag.H, true);
        }

        void RLCA() {
            clearFlags;
            setFlag(FFlag.C, cast(bool)(GPReg[GPRegister.A] & 0x80));
            ubyte bit0 = (GPReg[GPRegister.A] >> 7) & 0x01;
            GPReg[GPRegister.A] = cast(ubyte)((GPReg[GPRegister.A] << 1) | bit0);
        }

        void RRCA() {
            clearFlags;
            ubyte newBit7 = cast(ubyte)(GPReg[GPRegister.A] << 7);
            setFlag(FFlag.C, GPReg[GPRegister.A] & 0x01);
            GPReg[GPRegister.A] = ((GPReg[GPRegister.A] >> 1) & 0x7F) | newBit7;
        }

        void RLC(ref byte value) {
            clearFlags;
            setFlag(FFlag.C, cast(bool)(value & 0x80));
            byte newBit = (value >> 7) & 0x01;
            value = cast(byte)((value << 1) | newBit);
            setFlag(FFlag.Z, !value);
        }

        void RLC(ushort address) {
            byte value = mmu.read!byte(address);
            RLC(value);
            mmu.write(address, value);
        }

        void RRC(ref byte value) {
            clearFlags;
            setFlag(FFlag.C, cast(bool)(value & 0x01));
            byte newBit = cast(byte)((value << 7) & 0x80);
            value = cast(byte)(((value >> 1) & 0x7F) | newBit);
            setFlag(FFlag.Z, !value);
        }

        void RRC(ushort address) {
            byte value = mmu.read!byte(address);
            RRC(value);
            mmu.write(address, value);
        }

        void RL(ref byte value) {
            byte newBit = (GPReg[GPRegister.F] & FFlag.C) ? cast(byte)0x01 : cast(byte)0x00;
            clearFlags;
            setFlag(FFlag.C, cast(bool)(value & 0x80));
            value = cast(byte)((value << 1) | newBit);
            setFlag(FFlag.Z, !value);
        }

        void RLA() {
            RL(GPReg[GPRegister.A]);
            setFlag(FFlag.Z, false);
        }

        void RL(ushort address) {
            byte value = mmu.read!byte(address);
            RL(value);
            mmu.write(address, value);
        }

        void RR(ref byte value) {
            byte newBit = (GPReg[GPRegister.F] & FFlag.C) ? cast(byte)0x80 : cast(byte)0x00;
            clearFlags;
            setFlag(FFlag.C, value & 0x01);
            value = ((value >> 1) & 0x7F) | newBit;
            setFlag(FFlag.Z, !value);
        }

        void RRA() {
            RR(GPReg[GPRegister.A]);
            setFlag(FFlag.Z, false);
        }

        void RR(ushort address) {
            byte value = mmu.read!byte(address);
            RR(value);
            mmu.write(address, value);
        }
        
        void SRL(ref byte value) {
            clearFlags;
            setFlag(FFlag.C, value & 0x01);
            value = (value >> 1) & 0x7F;
            setFlag(FFlag.Z, !value);
        }

        void SRL(ushort address) {
            byte value = mmu.read!byte(address);
            SRL(value);
            mmu.write(address, value);
        }

        void SLA(ref byte value) {
            clearFlags;
            setFlag(FFlag.C, cast(bool)(value & 0x80));
            value <<= 1;
            setFlag(FFlag.Z, !value);
        }

        void SLA(ushort address) {
            byte value = mmu.read!byte(address);
            SLA(value);
            mmu.write(address, value);
        }

        void SRA(ref byte value) {
            clearFlags;
            setFlag(FFlag.C, cast(bool)(value & 0x01));
            value >>= 1;
            setFlag(FFlag.Z, !value);
        }

        void SRA(ushort address) {
            byte value = mmu.read!byte(address);
            SRA(value);
            mmu.write(address, value);
        }

        void RES(int bit, ref byte value) {
            value &= ~(1 << bit);
        }

        void RES(int bit, GPRegister reg1, GPRegister reg2) {
            mmu.write(doubleReg(reg1, reg2),
                      cast(ubyte)(mmu.read!byte(doubleReg(reg1, reg2)) & (~(1 << bit))));
        }

        void BIT(int bit, byte value) {
            setFlag(FFlag.Z, !cast(bool)((value >> bit) & 0x1));
            setFlag(FFlag.N, false);
            setFlag(FFlag.H, true );
        }

        void JP(bool condition = true) {
            if (condition) {
                PC = mmu.read!ushort(PC);
            } else {
                PC += 2;
            }
        }

        void JR(bool condition = true) {
            if (condition) {
                PC += mmu.read!byte(PC);
            }
            PC++;
        }

        void CALL(bool condition = true) {
            if (condition) {
                SP -= 2;
                mmu.write(SP, cast(ushort)(PC+2));
                PC = mmu.read!ushort(PC);
            } else {
                PC += 2;
            }
        }

        void CALL(ushort address, bool condition = true) {
            if (condition) {
                SP -= 2;
                mmu.write(SP, PC);
                PC = address;
            }
        }
        
        void RET(bool condition = true) {
            if (condition) {
                PC = mmu.read!ushort(SP);
                SP += 2;
            }
        }

        void CP(byte value) {
            byte result = cast(byte)(GPReg[GPRegister.A] - value);
        
            setFlag(FFlag.Z, !result);
            setFlag(FFlag.N, true);
            setFlag(FFlag.H, (GPReg[GPRegister.A] & 0xF) - (value & 0xF) < 0);
            setFlag(FFlag.C, ubyte(GPReg[GPRegister.A]) < ubyte(value));
        }

        void STOP() {
            if (type == GBType.GBC) {
                //Update double speed mode
                ubyte speed = mmu.read!ubyte(ExtraReg.GBCSpeedx2);
                if (speed & 0x1) {
                    if (speed & 0x80) {
                        //Exit double speed mode
                        mmu.rawRead!ubyte(ExtraReg.GBCSpeedx2) = 0x00;
                        clockSpeed /= 2;
                    } else {
                        //Enter double speed mode
                        mmu.rawRead!ubyte(ExtraReg.GBCSpeedx2) = 0x80;
                        clockSpeed *= 2;
                    }
                }
            }
        }

        //General functions
        void printDebug() {
            bool savedDebugMode = mmu.debugMode;
            mmu.debugMode = false;

            write("CPU : GP Registers(");
            foreach (i,e; GPReg) {
                writef("%s: %02x", cast(GPRegister)i, e);
                if (i != GPRegister.L) {
                    write(", ");
                }
            }
            writefln("), PC(%04x), SP(%04x), IFLAGS(%02x), Data adjacent to PC(%(%02x %)), Data adjacent to SP(%(%02x %)), %s, %s instructions executed so far", PC, SP, mmu.read!ubyte(ExtraReg.IFLAGS), mmu.read!(ubyte[16])(PC),
                                                                                                                                                                 mmu.read!(ubyte[16])(SP), interruptsEnabled ? "Interrupts are enabled" : "Interrupts are disabled",
                                                                                                                                                                 instCount);

            mmu.debugMode = savedDebugMode;
        }

    public:
        void reset(GBType type, bool dbg = false, MMU newMMU = null) {
            //Reset the registers
            GPReg = [ubyte(0x01), ubyte(0x00), ubyte(0x13), ubyte(0x00), ubyte(0xD8), ubyte(0xB0), ubyte(0x01), ubyte(0x4D)];
            if (type == GBType.GBC) {
                GPReg[GPRegister.A] = 0x11;
            }
            SP = 0xFFFE;
            PC = 0x100;

            //Other member variables
            debugMode = dbg;
            debugOutput = true;
            if (newMMU) {
                mmu = newMMU;
            }
        }

        void halt() {
            breakPC = 0;
        }

        size_t update(size_t time) {
            //Execute another instruction
            if (!halted) {
                //Read, decode, execute
                ubyte op = mmu.read!ubyte(PC++);

                switch (op) {
                    case 0x00:                                                                             cycleTimer = 4 ; break;
                    case 0x01: LD(GPRegister.B, GPRegister.C)                                            ; cycleTimer = 12; break;
                    case 0x02: mmu.write(doubleReg(GPRegister.B, GPRegister.C), GPReg[GPRegister.A])     ; cycleTimer = 8 ; break;
                    case 0x03: INC(GPRegister.B, GPRegister.C)                                           ; cycleTimer = 8 ; break;
                    case 0x04: INC(GPReg[GPRegister.B])                                                  ; cycleTimer = 4 ; break;
                    case 0x05: DEC(GPReg[GPRegister.B])                                                  ; cycleTimer = 4 ; break;
                    case 0x06: LD(GPRegister.B)                                                          ; cycleTimer = 8 ; break;
                    case 0x07: RLCA                                                                      ; cycleTimer = 4 ; break;
                    case 0x08: mmu.write(mmu.read!ushort(PC), SP); PC += 2                               ; cycleTimer = 20; break;
                    case 0x09: ADD(doubleReg(GPRegister.B, GPRegister.C))                                ; cycleTimer = 8 ; break;
                    case 0x0A: GPReg[GPRegister.A] = mmu.read!byte(doubleReg(GPRegister.B, GPRegister.C)); cycleTimer = 8 ; break;
                    case 0x0B: DEC(GPRegister.B, GPRegister.C)                                           ; cycleTimer = 8 ; break;
                    case 0x0C: INC(GPReg[GPRegister.C])                                                  ; cycleTimer = 4 ; break;
                    case 0x0D: DEC(GPReg[GPRegister.C])                                                  ; cycleTimer = 4 ; break;
                    case 0x0E: LD(GPRegister.C)                                                          ; cycleTimer = 8 ; break;
                    case 0x0F: RRCA                                                                      ; cycleTimer = 4 ; break;
                    case 0x10: STOP                                                                      ; cycleTimer = 4 ; break;
                    case 0x11: LD(GPRegister.D, GPRegister.E)                                            ; cycleTimer = 12; break;
                    case 0x12: mmu.write(doubleReg(GPRegister.D, GPRegister.E), GPReg[GPRegister.A])     ; cycleTimer = 8 ; break;
                    case 0x13: INC(GPRegister.D, GPRegister.E)                                           ; cycleTimer = 8 ; break;
                    case 0x14: INC(GPReg[GPRegister.D])                                                  ; cycleTimer = 4 ; break;
                    case 0x15: DEC(GPReg[GPRegister.D])                                                  ; cycleTimer = 4 ; break;
                    case 0x16: LD(GPRegister.D)                                                          ; cycleTimer = 8 ; break;
                    case 0x17: RLA                                                                       ; cycleTimer = 4 ; break;
                    case 0x18: PC += mmu.read!byte(PC++)                                                 ; cycleTimer = 8 ; break;
                    case 0x19: ADD(doubleReg(GPRegister.D, GPRegister.E))                                ; cycleTimer = 8 ; break;
                    case 0x1A: GPReg[GPRegister.A] = mmu.read!byte(doubleReg(GPRegister.D, GPRegister.E)); cycleTimer = 8 ; break;
                    case 0x1B: DEC(GPRegister.D, GPRegister.E)                                           ; cycleTimer = 8 ; break;
                    case 0x1C: INC(GPReg[GPRegister.E])                                                  ; cycleTimer = 4 ; break;
                    case 0x1D: DEC(GPReg[GPRegister.E])                                                  ; cycleTimer = 4 ; break;
                    case 0x1E: LD(GPRegister.E)                                                          ; cycleTimer = 8 ; break;
                    case 0x1F: RRA                                                                       ; cycleTimer = 4 ; break;
                    case 0x20: JR(!(GPReg[GPRegister.F] & FFlag.Z))                                      ; cycleTimer = 8 ; break;
                    case 0x21: LD(GPRegister.H, GPRegister.L)                                            ; cycleTimer = 12; break;
                    case 0x22: LD(GPRegister.H,GPRegister.L,GPRegister.A); INC(GPRegister.H,GPRegister.L); cycleTimer = 8 ; break;
                    case 0x23: INC(GPRegister.H, GPRegister.L)                                           ; cycleTimer = 8 ; break;
                    case 0x24: INC(GPReg[GPRegister.H])                                                  ; cycleTimer = 4 ; break;
                    case 0x25: DEC(GPReg[GPRegister.H])                                                  ; cycleTimer = 4 ; break;
                    case 0x26: LD(GPRegister.H)                                                          ; cycleTimer = 8 ; break;
                    case 0x27: DAA                                                                       ; cycleTimer = 4 ; break;
                    case 0x28: JR(cast(bool)(GPReg[GPRegister.F] & FFlag.Z))                             ; cycleTimer = 8 ; break;
                    case 0x29: ADD(doubleReg(GPRegister.H, GPRegister.L))                                ; cycleTimer = 8 ; break;
                    case 0x2A: GPReg[GPRegister.A] = mmu.read!byte(doubleReg(GPRegister.H, GPRegister.L));
                               INC(GPRegister.H, GPRegister.L)                                           ; cycleTimer = 8 ; break;
                    case 0x2B: DEC(GPRegister.H, GPRegister.L)                                           ; cycleTimer = 8 ; break;
                    case 0x2C: INC(GPReg[GPRegister.L])                                                  ; cycleTimer = 4 ; break;
                    case 0x2D: DEC(GPReg[GPRegister.L])                                                  ; cycleTimer = 4 ; break;
                    case 0x2E: LD(GPRegister.L)                                                          ; cycleTimer = 8 ; break;
                    case 0x2F: CPL                                                                       ; cycleTimer = 4 ; break;
                    case 0x30: JR(!(GPReg[GPRegister.F] & FFlag.C))                                      ; cycleTimer = 8 ; break;
                    case 0x31: SP = mmu.read!ushort(PC); PC += 2                                         ; cycleTimer = 12; break;
                    case 0x32: LD(GPRegister.H,GPRegister.L,GPRegister.A); DEC(GPRegister.H,GPRegister.L); cycleTimer = 8 ; break;
                    case 0x33: ++SP                                                                      ; cycleTimer = 8 ; break;
                    case 0x34: INC(doubleReg(GPRegister.H, GPRegister.L))                                ; cycleTimer = 12; break;  
                    case 0x35: DEC(doubleReg(GPRegister.H, GPRegister.L))                                ; cycleTimer = 12; break;
                    case 0x36: mmu.write(doubleReg(GPRegister.H, GPRegister.L), mmu.read!ubyte(PC++))    ; cycleTimer = 12; break;
                    case 0x37: SCF                                                                       ; cycleTimer = 4 ; break;
                    case 0x38: JR(cast(bool)(GPReg[GPRegister.F] & FFlag.C))                             ; cycleTimer = 8 ; break;
                    case 0x39: ADD(SP)                                                                   ; cycleTimer = 8 ; break;              
                    case 0x3A: GPReg[GPRegister.A] = mmu.read!byte(doubleReg(GPRegister.H, GPRegister.L));
                               DEC(GPRegister.H, GPRegister.L)                                           ; cycleTimer = 8 ; break;
                    case 0x3B: SP--                                                                      ; cycleTimer = 8 ; break;
                    case 0x3C: INC(GPReg[GPRegister.A])                                                  ; cycleTimer = 4 ; break;
                    case 0x3D: DEC(GPReg[GPRegister.A])                                                  ; cycleTimer = 4 ; break;
                    case 0x3E: LD(GPRegister.A)                                                          ; cycleTimer = 8 ; break;
                    case 0x3F: CCF                                                                       ; cycleTimer = 4 ; break;
                    case 0x40: GPReg[GPRegister.B] = GPReg[GPRegister.B]                                 ; cycleTimer = 4 ; break;
                    case 0x41: GPReg[GPRegister.B] = GPReg[GPRegister.C]                                 ; cycleTimer = 4 ; break;
                    case 0x42: GPReg[GPRegister.B] = GPReg[GPRegister.D]                                 ; cycleTimer = 4 ; break;
                    case 0x43: GPReg[GPRegister.B] = GPReg[GPRegister.E]                                 ; cycleTimer = 4 ; break;
                    case 0x44: GPReg[GPRegister.B] = GPReg[GPRegister.H]                                 ; cycleTimer = 4 ; break;
                    case 0x45: GPReg[GPRegister.B] = GPReg[GPRegister.L]                                 ; cycleTimer = 4 ; break;
                    case 0x46: GPReg[GPRegister.B] = mmu.read!byte(doubleReg(GPRegister.H, GPRegister.L)); cycleTimer = 8 ; break;
                    case 0x47: GPReg[GPRegister.B] = GPReg[GPRegister.A]                                 ; cycleTimer = 4 ; break;
                    case 0x48: GPReg[GPRegister.C] = GPReg[GPRegister.B]                                 ; cycleTimer = 4 ; break;
                    case 0x49: GPReg[GPRegister.C] = GPReg[GPRegister.C]                                 ; cycleTimer = 4 ; break;
                    case 0x4A: GPReg[GPRegister.C] = GPReg[GPRegister.D]                                 ; cycleTimer = 4 ; break;
                    case 0x4B: GPReg[GPRegister.C] = GPReg[GPRegister.E]                                 ; cycleTimer = 4 ; break;
                    case 0x4C: GPReg[GPRegister.C] = GPReg[GPRegister.H]                                 ; cycleTimer = 4 ; break;
                    case 0x4D: GPReg[GPRegister.C] = GPReg[GPRegister.L]                                 ; cycleTimer = 4 ; break;
                    case 0x4E: GPReg[GPRegister.C] = mmu.read!byte(doubleReg(GPRegister.H, GPRegister.L)); cycleTimer = 8 ; break;
                    case 0x4F: GPReg[GPRegister.C] = GPReg[GPRegister.A]                                 ; cycleTimer = 4 ; break;
                    case 0x50: GPReg[GPRegister.D] = GPReg[GPRegister.B]                                 ; cycleTimer = 4 ; break;
                    case 0x51: GPReg[GPRegister.D] = GPReg[GPRegister.C]                                 ; cycleTimer = 4 ; break;
                    case 0x52: GPReg[GPRegister.D] = GPReg[GPRegister.D]                                 ; cycleTimer = 4 ; break;
                    case 0x53: GPReg[GPRegister.D] = GPReg[GPRegister.E]                                 ; cycleTimer = 4 ; break;
                    case 0x54: GPReg[GPRegister.D] = GPReg[GPRegister.H]                                 ; cycleTimer = 4 ; break;
                    case 0x55: GPReg[GPRegister.D] = GPReg[GPRegister.L]                                 ; cycleTimer = 4 ; break;
                    case 0x56: GPReg[GPRegister.D] = mmu.read!byte(doubleReg(GPRegister.H, GPRegister.L)); cycleTimer = 8 ; break;
                    case 0x57: GPReg[GPRegister.D] = GPReg[GPRegister.A]                                 ; cycleTimer = 4 ; break;
                    case 0x58: GPReg[GPRegister.E] = GPReg[GPRegister.B]                                 ; cycleTimer = 4 ; break;
                    case 0x59: GPReg[GPRegister.E] = GPReg[GPRegister.C]                                 ; cycleTimer = 4 ; break;
                    case 0x5A: GPReg[GPRegister.E] = GPReg[GPRegister.D]                                 ; cycleTimer = 4 ; break;
                    case 0x5B: GPReg[GPRegister.E] = GPReg[GPRegister.E]                                 ; cycleTimer = 4 ; break;
                    case 0x5C: GPReg[GPRegister.E] = GPReg[GPRegister.H]                                 ; cycleTimer = 4 ; break;
                    case 0x5D: GPReg[GPRegister.E] = GPReg[GPRegister.L]                                 ; cycleTimer = 4 ; break;
                    case 0x5E: GPReg[GPRegister.E] = mmu.read!byte(doubleReg(GPRegister.H, GPRegister.L)); cycleTimer = 8 ; break;
                    case 0x5F: GPReg[GPRegister.E] = GPReg[GPRegister.A]                                 ; cycleTimer = 4 ; break;
                    case 0x60: GPReg[GPRegister.H] = GPReg[GPRegister.B]                                 ; cycleTimer = 4 ; break;
                    case 0x61: GPReg[GPRegister.H] = GPReg[GPRegister.C]                                 ; cycleTimer = 4 ; break;
                    case 0x62: GPReg[GPRegister.H] = GPReg[GPRegister.D]                                 ; cycleTimer = 4 ; break;
                    case 0x63: GPReg[GPRegister.H] = GPReg[GPRegister.E]                                 ; cycleTimer = 4 ; break;
                    case 0x64: GPReg[GPRegister.H] = GPReg[GPRegister.H]                                 ; cycleTimer = 4 ; break;
                    case 0x65: GPReg[GPRegister.H] = GPReg[GPRegister.L]                                 ; cycleTimer = 4 ; break;
                    case 0x66: GPReg[GPRegister.H] = mmu.read!byte(doubleReg(GPRegister.H, GPRegister.L)); cycleTimer = 8 ; break;
                    case 0x67: GPReg[GPRegister.H] = GPReg[GPRegister.A]                                 ; cycleTimer = 4 ; break;
                    case 0x68: GPReg[GPRegister.L] = GPReg[GPRegister.B]                                 ; cycleTimer = 4 ; break;
                    case 0x69: GPReg[GPRegister.L] = GPReg[GPRegister.C]                                 ; cycleTimer = 4 ; break;
                    case 0x6A: GPReg[GPRegister.L] = GPReg[GPRegister.D]                                 ; cycleTimer = 4 ; break;
                    case 0x6B: GPReg[GPRegister.L] = GPReg[GPRegister.E]                                 ; cycleTimer = 4 ; break;
                    case 0x6C: GPReg[GPRegister.L] = GPReg[GPRegister.H]                                 ; cycleTimer = 4 ; break;
                    case 0x6D: GPReg[GPRegister.L] = GPReg[GPRegister.L]                                 ; cycleTimer = 4 ; break;
                    case 0x6E: GPReg[GPRegister.L] = mmu.read!byte(doubleReg(GPRegister.H, GPRegister.L)); cycleTimer = 8 ; break;
                    case 0x6F: GPReg[GPRegister.L] = GPReg[GPRegister.A]                                 ; cycleTimer = 4 ; break;
                    case 0x70: mmu.write(doubleReg(GPRegister.H, GPRegister.L), GPReg[GPRegister.B])     ; cycleTimer = 8 ; break;
                    case 0x71: mmu.write(doubleReg(GPRegister.H, GPRegister.L), GPReg[GPRegister.C])     ; cycleTimer = 8 ; break;
                    case 0x72: mmu.write(doubleReg(GPRegister.H, GPRegister.L), GPReg[GPRegister.D])     ; cycleTimer = 8 ; break;
                    case 0x73: mmu.write(doubleReg(GPRegister.H, GPRegister.L), GPReg[GPRegister.E])     ; cycleTimer = 8 ; break;
                    case 0x74: mmu.write(doubleReg(GPRegister.H, GPRegister.L), GPReg[GPRegister.H])     ; cycleTimer = 8 ; break;
                    case 0x75: mmu.write(doubleReg(GPRegister.H, GPRegister.L), GPReg[GPRegister.L])     ; cycleTimer = 8 ; break;
                    case 0x76: halted = true                                                             ; cycleTimer = 4 ; break;
                    case 0x77: mmu.write(doubleReg(GPRegister.H, GPRegister.L), GPReg[GPRegister.A])     ; cycleTimer = 8 ; break;
                    case 0x78: GPReg[GPRegister.A] = GPReg[GPRegister.B]                                 ; cycleTimer = 4 ; break;
                    case 0x79: GPReg[GPRegister.A] = GPReg[GPRegister.C]                                 ; cycleTimer = 4 ; break;
                    case 0x7A: GPReg[GPRegister.A] = GPReg[GPRegister.D]                                 ; cycleTimer = 4 ; break;
                    case 0x7B: GPReg[GPRegister.A] = GPReg[GPRegister.E]                                 ; cycleTimer = 4 ; break;
                    case 0x7C: GPReg[GPRegister.A] = GPReg[GPRegister.H]                                 ; cycleTimer = 4 ; break;
                    case 0x7D: GPReg[GPRegister.A] = GPReg[GPRegister.L]                                 ; cycleTimer = 4 ; break;
                    case 0x7E: GPReg[GPRegister.A] = mmu.read!byte(doubleReg(GPRegister.H, GPRegister.L)); cycleTimer = 8 ; break;
                    case 0x7F: GPReg[GPRegister.A] = GPReg[GPRegister.A]                                 ; cycleTimer = 4 ; break;
                    case 0x80: ADD(GPReg[GPRegister.B])                                                  ; cycleTimer = 4 ; break;
                    case 0x81: ADD(GPReg[GPRegister.C])                                                  ; cycleTimer = 4 ; break;
                    case 0x82: ADD(GPReg[GPRegister.D])                                                  ; cycleTimer = 4 ; break;                
                    case 0x83: ADD(GPReg[GPRegister.E])                                                  ; cycleTimer = 4 ; break;
                    case 0x84: ADD(GPReg[GPRegister.H])                                                  ; cycleTimer = 4 ; break;
                    case 0x85: ADD(GPReg[GPRegister.L])                                                  ; cycleTimer = 4 ; break;
                    case 0x86: ADD(mmu.read!byte(doubleReg(GPRegister.H, GPRegister.L)))                 ; cycleTimer = 8 ; break;
                    case 0x87: ADD(GPReg[GPRegister.A])                                                  ; cycleTimer = 4 ; break;
                    case 0x88: ADDC(GPReg[GPRegister.B])                                                 ; cycleTimer = 4 ; break;
                    case 0x89: ADDC(GPReg[GPRegister.C])                                                 ; cycleTimer = 4 ; break;
                    case 0x8A: ADDC(GPReg[GPRegister.D])                                                 ; cycleTimer = 4 ; break;
                    case 0x8B: ADDC(GPReg[GPRegister.E])                                                 ; cycleTimer = 4 ; break;
                    case 0x8C: ADDC(GPReg[GPRegister.H])                                                 ; cycleTimer = 4 ; break;
                    case 0x8D: ADDC(GPReg[GPRegister.L])                                                 ; cycleTimer = 4 ; break;
                    case 0x8E: ADDC(mmu.read!byte(doubleReg(GPRegister.H, GPRegister.L)))                ; cycleTimer = 8 ; break;
                    case 0x8F: ADDC(GPReg[GPRegister.A])                                                 ; cycleTimer = 4 ; break;
                    case 0x90: SUB(GPReg[GPRegister.B])                                                  ; cycleTimer = 4 ; break;
                    case 0x91: SUB(GPReg[GPRegister.C])                                                  ; cycleTimer = 4 ; break;
                    case 0x92: SUB(GPReg[GPRegister.D])                                                  ; cycleTimer = 4 ; break;                    
                    case 0x93: SUB(GPReg[GPRegister.E])                                                  ; cycleTimer = 4 ; break;
                    case 0x94: SUB(GPReg[GPRegister.H])                                                  ; cycleTimer = 4 ; break;
                    case 0x95: SUB(GPReg[GPRegister.L])                                                  ; cycleTimer = 4 ; break;
                    case 0x96: SUB(mmu.read!byte(doubleReg(GPRegister.H, GPRegister.L)))                 ; cycleTimer = 4 ; break;
                    case 0x97: SUB(GPReg[GPRegister.A])                                                  ; cycleTimer = 4 ; break;
                    case 0x98: SUBC(GPReg[GPRegister.B])                                                 ; cycleTimer = 4 ; break;
                    case 0x99: SUBC(GPReg[GPRegister.C])                                                 ; cycleTimer = 4 ; break;
                    case 0x9A: SUBC(GPReg[GPRegister.D])                                                 ; cycleTimer = 4 ; break;
                    case 0x9B: SUBC(GPReg[GPRegister.E])                                                 ; cycleTimer = 4 ; break;
                    case 0x9C: SUBC(GPReg[GPRegister.H])                                                 ; cycleTimer = 4 ; break;
                    case 0x9D: SUBC(GPReg[GPRegister.L])                                                 ; cycleTimer = 4 ; break;
                    case 0x9E: SUBC(mmu.read!byte(doubleReg(GPRegister.H, GPRegister.L)))                ; cycleTimer = 4 ; break;
                    case 0x9F: SUBC(GPReg[GPRegister.A])                                                 ; cycleTimer = 4 ; break;
                    case 0xA0: AND(GPReg[GPRegister.B])                                                  ; cycleTimer = 4 ; break;
                    case 0xA1: AND(GPReg[GPRegister.C])                                                  ; cycleTimer = 4 ; break;
                    case 0xA2: AND(GPReg[GPRegister.D])                                                  ; cycleTimer = 4 ; break;
                    case 0xA3: AND(GPReg[GPRegister.E])                                                  ; cycleTimer = 4 ; break;
                    case 0xA4: AND(GPReg[GPRegister.H])                                                  ; cycleTimer = 4 ; break;
                    case 0xA5: AND(GPReg[GPRegister.L])                                                  ; cycleTimer = 4 ; break;
                    case 0xA6: AND(mmu.read!byte(doubleReg(GPRegister.H, GPRegister.L)))                 ; cycleTimer = 8 ; break;
                    case 0xA7: AND(GPReg[GPRegister.A])                                                  ; cycleTimer = 4 ; break;
                    case 0xA8: XOR(GPReg[GPRegister.B])                                                  ; cycleTimer = 4 ; break;
                    case 0xA9: XOR(GPReg[GPRegister.C])                                                  ; cycleTimer = 4 ; break;
                    case 0xAA: XOR(GPReg[GPRegister.D])                                                  ; cycleTimer = 4 ; break;
                    case 0xAB: XOR(GPReg[GPRegister.E])                                                  ; cycleTimer = 4 ; break;
                    case 0xAC: XOR(GPReg[GPRegister.H])                                                  ; cycleTimer = 4 ; break;
                    case 0xAD: XOR(GPReg[GPRegister.L])                                                  ; cycleTimer = 4 ; break;
                    case 0xAE: XOR(mmu.read!byte(doubleReg(GPRegister.H, GPRegister.L)))                 ; cycleTimer = 8 ; break;
                    case 0xAF: XOR(GPReg[GPRegister.A])                                                  ; cycleTimer = 4 ; break;
                    case 0xB0: OR(GPReg[GPRegister.B])                                                   ; cycleTimer = 4 ; break;
                    case 0xB1: OR(GPReg[GPRegister.C])                                                   ; cycleTimer = 4 ; break;
                    case 0xB2: OR(GPReg[GPRegister.D])                                                   ; cycleTimer = 4 ; break;
                    case 0xB3: OR(GPReg[GPRegister.E])                                                   ; cycleTimer = 4 ; break;
                    case 0xB4: OR(GPReg[GPRegister.H])                                                   ; cycleTimer = 4 ; break;
                    case 0xB5: OR(GPReg[GPRegister.L])                                                   ; cycleTimer = 4 ; break;
                    case 0xB6: OR(mmu.read!byte(doubleReg(GPRegister.H, GPRegister.L)))                  ; cycleTimer = 8 ; break;
                    case 0xB7: OR(GPReg[GPRegister.A])                                                   ; cycleTimer = 4 ; break;
                    case 0xB8: CP(GPReg[GPRegister.B])                                                   ; cycleTimer = 4 ; break;
                    case 0xB9: CP(GPReg[GPRegister.C])                                                   ; cycleTimer = 4 ; break;
                    case 0xBA: CP(GPReg[GPRegister.D])                                                   ; cycleTimer = 4 ; break;
                    case 0xBB: CP(GPReg[GPRegister.E])                                                   ; cycleTimer = 4 ; break;
                    case 0xBC: CP(GPReg[GPRegister.H])                                                   ; cycleTimer = 4 ; break;
                    case 0xBD: CP(GPReg[GPRegister.L])                                                   ; cycleTimer = 4 ; break;
                    case 0xBE: CP(mmu.read!byte(doubleReg(GPRegister.H, GPRegister.L)))                  ; cycleTimer = 8 ; break;
                    case 0xBF: CP(GPReg[GPRegister.A])                                                   ; cycleTimer = 8 ; break;
                    case 0xC0: RET(!(GPReg[GPRegister.F] & FFlag.Z))                                     ; cycleTimer = 8 ; break;
                    case 0xC1: POP(GPRegister.B, GPRegister.C)                                           ; cycleTimer = 12; break;
                    case 0xC2: JP(!(GPReg[GPRegister.F] & FFlag.Z))                                      ; cycleTimer = 12; break;
                    case 0xC3: PC = mmu.read!ushort(PC)                                                  ; cycleTimer = 12; break;
                    case 0xC4: CALL(!(GPReg[GPRegister.F] & FFlag.Z))                                    ; cycleTimer = 12; break;
                    case 0xC5: PUSH(GPRegister.B, GPRegister.C)                                          ; cycleTimer = 16; break;
                    case 0xC6: ADD(mmu.read!byte(PC++))                                                  ; cycleTimer = 8 ; break;
                    case 0xC7: CALL(ushort(0))                                                           ; cycleTimer = 32; break;
                    case 0xC8: RET(cast(bool)(GPReg[GPRegister.F] & FFlag.Z))                            ; cycleTimer = 8 ; break;
                    case 0xC9: RET                                                                       ; cycleTimer = 8 ; break;
                    case 0xCA: JP(cast(bool)(GPReg[GPRegister.F] & FFlag.Z))                             ; cycleTimer = 12; break;
                    case 0xCB: 
                        switch (mmu.read!ubyte(PC++)) {
                            case 0x00: RLC(GPReg[GPRegister.B])                                                ; cycleTimer = 8 ; break;
                            case 0x01: RLC(GPReg[GPRegister.C])                                                ; cycleTimer = 8 ; break;
                            case 0x02: RLC(GPReg[GPRegister.D])                                                ; cycleTimer = 8 ; break;
                            case 0x03: RLC(GPReg[GPRegister.E])                                                ; cycleTimer = 8 ; break;
                            case 0x04: RLC(GPReg[GPRegister.H])                                                ; cycleTimer = 8 ; break;
                            case 0x05: RLC(GPReg[GPRegister.L])                                                ; cycleTimer = 8 ; break;
                            case 0x06: RLC(doubleReg(GPRegister.H, GPRegister.L))                              ; cycleTimer = 16; break;
                            case 0x07: RLC(GPReg[GPRegister.A])                                                ; cycleTimer = 8 ; break;
                            case 0x08: RRC(GPReg[GPRegister.B])                                                ; cycleTimer = 8 ; break;
                            case 0x09: RRC(GPReg[GPRegister.C])                                                ; cycleTimer = 8 ; break;
                            case 0x0A: RRC(GPReg[GPRegister.D])                                                ; cycleTimer = 8 ; break;
                            case 0x0B: RRC(GPReg[GPRegister.E])                                                ; cycleTimer = 8 ; break;
                            case 0x0C: RRC(GPReg[GPRegister.H])                                                ; cycleTimer = 8 ; break;
                            case 0x0D: RRC(GPReg[GPRegister.L])                                                ; cycleTimer = 8 ; break;
                            case 0x0E: RRC(doubleReg(GPRegister.H, GPRegister.L))                              ; cycleTimer = 16; break;
                            case 0x0F: RRC(GPReg[GPRegister.A])                                                ; cycleTimer = 8 ; break;
                            case 0x10: RL(GPReg[GPRegister.B])                                                 ; cycleTimer = 8 ; break;
                            case 0x11: RL(GPReg[GPRegister.C])                                                 ; cycleTimer = 8 ; break;
                            case 0x12: RL(GPReg[GPRegister.D])                                                 ; cycleTimer = 8 ; break;
                            case 0x13: RL(GPReg[GPRegister.E])                                                 ; cycleTimer = 8 ; break;
                            case 0x14: RL(GPReg[GPRegister.H])                                                 ; cycleTimer = 8 ; break;
                            case 0x15: RL(GPReg[GPRegister.L])                                                 ; cycleTimer = 8 ; break;
                            case 0x16: RL(doubleReg(GPRegister.H, GPRegister.L))                               ; cycleTimer = 16; break;
                            case 0x17: RL(GPReg[GPRegister.A])                                                 ; cycleTimer = 8 ; break;
                            case 0x18: RR(GPReg[GPRegister.B])                                                 ; cycleTimer = 8 ; break;
                            case 0x19: RR(GPReg[GPRegister.C])                                                 ; cycleTimer = 8 ; break;
                            case 0x1A: RR(GPReg[GPRegister.D])                                                 ; cycleTimer = 8 ; break;
                            case 0x1B: RR(GPReg[GPRegister.E])                                                 ; cycleTimer = 8 ; break;
                            case 0x1C: RR(GPReg[GPRegister.H])                                                 ; cycleTimer = 8 ; break;
                            case 0x1D: RR(GPReg[GPRegister.L])                                                 ; cycleTimer = 8 ; break;
                            case 0x1E: RR(doubleReg(GPRegister.H, GPRegister.L))                               ; cycleTimer = 16; break;
                            case 0x1F: RR(GPReg[GPRegister.A])                                                 ; cycleTimer = 8 ; break;
                            case 0x20: SLA(GPReg[GPRegister.B])                                                ; cycleTimer = 8 ; break;
                            case 0x21: SLA(GPReg[GPRegister.C])                                                ; cycleTimer = 8 ; break;
                            case 0x22: SLA(GPReg[GPRegister.D])                                                ; cycleTimer = 8 ; break;
                            case 0x23: SLA(GPReg[GPRegister.E])                                                ; cycleTimer = 8 ; break;
                            case 0x24: SLA(GPReg[GPRegister.H])                                                ; cycleTimer = 8 ; break;
                            case 0x25: SLA(GPReg[GPRegister.L])                                                ; cycleTimer = 8 ; break;
                            case 0x26: SLA(doubleReg(GPRegister.H, GPRegister.L))                              ; cycleTimer = 16; break;
                            case 0x27: SLA(GPReg[GPRegister.A])                                                ; cycleTimer = 8 ; break;
                            case 0x28: SRA(GPReg[GPRegister.B])                                                ; cycleTimer = 8 ; break;
                            case 0x29: SRA(GPReg[GPRegister.C])                                                ; cycleTimer = 8 ; break;
                            case 0x2A: SRA(GPReg[GPRegister.D])                                                ; cycleTimer = 8 ; break;
                            case 0x2B: SRA(GPReg[GPRegister.E])                                                ; cycleTimer = 8 ; break;
                            case 0x2C: SRA(GPReg[GPRegister.H])                                                ; cycleTimer = 8 ; break;
                            case 0x2D: SRA(GPReg[GPRegister.L])                                                ; cycleTimer = 8 ; break;
                            case 0x2E: SRA(doubleReg(GPRegister.H, GPRegister.L))                              ; cycleTimer = 16; break;
                            case 0x2F: SRA(GPReg[GPRegister.A])                                                ; cycleTimer = 8 ; break;
                            case 0x30: SWAP(GPReg[GPRegister.B])                                               ; cycleTimer = 8 ; break;
                            case 0x31: SWAP(GPReg[GPRegister.C])                                               ; cycleTimer = 8 ; break;
                            case 0x32: SWAP(GPReg[GPRegister.D])                                               ; cycleTimer = 8 ; break;
                            case 0x33: SWAP(GPReg[GPRegister.E])                                               ; cycleTimer = 8 ; break;
                            case 0x34: SWAP(GPReg[GPRegister.H])                                               ; cycleTimer = 8 ; break;
                            case 0x35: SWAP(GPReg[GPRegister.L])                                               ; cycleTimer = 8 ; break;
                            case 0x36: SWAP(doubleReg(GPRegister.H, GPRegister.L))                             ; cycleTimer = 16; break;
                            case 0x37: SWAP(GPReg[GPRegister.A])                                               ; cycleTimer = 8 ; break;
                            case 0x38: SRL(GPReg[GPRegister.B])                                                ; cycleTimer = 8 ; break;
                            case 0x39: SRL(GPReg[GPRegister.C])                                                ; cycleTimer = 8 ; break;
                            case 0x3A: SRL(GPReg[GPRegister.D])                                                ; cycleTimer = 8 ; break;
                            case 0x3B: SRL(GPReg[GPRegister.E])                                                ; cycleTimer = 8 ; break;
                            case 0x3C: SRL(GPReg[GPRegister.H])                                                ; cycleTimer = 8 ; break;
                            case 0x3D: SRL(GPReg[GPRegister.L])                                                ; cycleTimer = 8 ; break;
                            case 0x3E: SRL(doubleReg(GPRegister.H, GPRegister.L))                              ; cycleTimer = 16; break;
                            case 0x3F: SRL(GPReg[GPRegister.A])                                                ; cycleTimer = 8 ; break;
                            case 0x40: BIT(0, GPReg[GPRegister.B])                                             ; cycleTimer = 8 ; break;
                            case 0x41: BIT(0, GPReg[GPRegister.C])                                             ; cycleTimer = 8 ; break;
                            case 0x42: BIT(0, GPReg[GPRegister.D])                                             ; cycleTimer = 8 ; break;
                            case 0x43: BIT(0, GPReg[GPRegister.E])                                             ; cycleTimer = 8 ; break;
                            case 0x44: BIT(0, GPReg[GPRegister.H])                                             ; cycleTimer = 8 ; break;
                            case 0x45: BIT(0, GPReg[GPRegister.L])                                             ; cycleTimer = 8 ; break;
                            case 0x46: BIT(0, mmu.read!byte(doubleReg(GPRegister.H, GPRegister.L)))            ; cycleTimer = 16; break;
                            case 0x47: BIT(0, GPReg[GPRegister.A])                                             ; cycleTimer = 8 ; break;
                            case 0x48: BIT(1, GPReg[GPRegister.B])                                             ; cycleTimer = 8 ; break;
                            case 0x49: BIT(1, GPReg[GPRegister.C])                                             ; cycleTimer = 8 ; break;
                            case 0x4A: BIT(1, GPReg[GPRegister.D])                                             ; cycleTimer = 8 ; break;
                            case 0x4B: BIT(1, GPReg[GPRegister.E])                                             ; cycleTimer = 8 ; break;
                            case 0x4C: BIT(1, GPReg[GPRegister.H])                                             ; cycleTimer = 8 ; break;
                            case 0x4D: BIT(1, GPReg[GPRegister.L])                                             ; cycleTimer = 8 ; break;
                            case 0x4E: BIT(1, mmu.read!byte(doubleReg(GPRegister.H, GPRegister.L)))            ; cycleTimer = 16; break;
                            case 0x4F: BIT(1, GPReg[GPRegister.A])                                             ; cycleTimer = 8 ; break;
                            case 0x50: BIT(2, GPReg[GPRegister.B])                                             ; cycleTimer = 8 ; break;
                            case 0x51: BIT(2, GPReg[GPRegister.C])                                             ; cycleTimer = 8 ; break;
                            case 0x52: BIT(2, GPReg[GPRegister.D])                                             ; cycleTimer = 8 ; break;
                            case 0x53: BIT(2, GPReg[GPRegister.E])                                             ; cycleTimer = 8 ; break;
                            case 0x54: BIT(2, GPReg[GPRegister.H])                                             ; cycleTimer = 8 ; break;
                            case 0x55: BIT(2, GPReg[GPRegister.L])                                             ; cycleTimer = 8 ; break;
                            case 0x56: BIT(2, mmu.read!byte(doubleReg(GPRegister.H, GPRegister.L)))            ; cycleTimer = 16; break;
                            case 0x57: BIT(2, GPReg[GPRegister.A])                                             ; cycleTimer = 8 ; break;
                            case 0x58: BIT(3, GPReg[GPRegister.B])                                             ; cycleTimer = 8 ; break;
                            case 0x59: BIT(3, GPReg[GPRegister.C])                                             ; cycleTimer = 8 ; break;
                            case 0x5A: BIT(3, GPReg[GPRegister.D])                                             ; cycleTimer = 8 ; break;
                            case 0x5B: BIT(3, GPReg[GPRegister.E])                                             ; cycleTimer = 8 ; break;
                            case 0x5C: BIT(3, GPReg[GPRegister.H])                                             ; cycleTimer = 8 ; break;
                            case 0x5D: BIT(3, GPReg[GPRegister.L])                                             ; cycleTimer = 8 ; break;
                            case 0x5E: BIT(3, mmu.read!byte(doubleReg(GPRegister.H, GPRegister.L)))            ; cycleTimer = 16; break;
                            case 0x5F: BIT(3, GPReg[GPRegister.A])                                             ; cycleTimer = 8 ; break;
                            case 0x60: BIT(4, GPReg[GPRegister.B])                                             ; cycleTimer = 8 ; break;
                            case 0x61: BIT(4, GPReg[GPRegister.C])                                             ; cycleTimer = 8 ; break;
                            case 0x62: BIT(4, GPReg[GPRegister.D])                                             ; cycleTimer = 8 ; break;
                            case 0x63: BIT(4, GPReg[GPRegister.E])                                             ; cycleTimer = 8 ; break;
                            case 0x64: BIT(4, GPReg[GPRegister.H])                                             ; cycleTimer = 8 ; break;
                            case 0x65: BIT(4, GPReg[GPRegister.L])                                             ; cycleTimer = 8 ; break;
                            case 0x66: BIT(4, mmu.read!byte(doubleReg(GPRegister.H, GPRegister.L)))            ; cycleTimer = 16; break;
                            case 0x67: BIT(4, GPReg[GPRegister.A])                                             ; cycleTimer = 8 ; break;
                            case 0x68: BIT(5, GPReg[GPRegister.B])                                             ; cycleTimer = 8 ; break;
                            case 0x69: BIT(5, GPReg[GPRegister.C])                                             ; cycleTimer = 8 ; break;
                            case 0x6A: BIT(5, GPReg[GPRegister.D])                                             ; cycleTimer = 8 ; break;
                            case 0x6B: BIT(5, GPReg[GPRegister.E])                                             ; cycleTimer = 8 ; break;
                            case 0x6C: BIT(5, GPReg[GPRegister.H])                                             ; cycleTimer = 8 ; break;
                            case 0x6D: BIT(5, GPReg[GPRegister.L])                                             ; cycleTimer = 8 ; break;
                            case 0x6E: BIT(5, mmu.read!byte(doubleReg(GPRegister.H, GPRegister.L)))            ; cycleTimer = 16; break;
                            case 0x6F: BIT(5, GPReg[GPRegister.A])                                             ; cycleTimer = 8 ; break;
                            case 0x70: BIT(6, GPReg[GPRegister.B])                                             ; cycleTimer = 8 ; break;
                            case 0x71: BIT(6, GPReg[GPRegister.C])                                             ; cycleTimer = 8 ; break;
                            case 0x72: BIT(6, GPReg[GPRegister.D])                                             ; cycleTimer = 8 ; break;
                            case 0x73: BIT(6, GPReg[GPRegister.E])                                             ; cycleTimer = 8 ; break;
                            case 0x74: BIT(6, GPReg[GPRegister.H])                                             ; cycleTimer = 8 ; break;
                            case 0x75: BIT(6, GPReg[GPRegister.L])                                             ; cycleTimer = 8 ; break;
                            case 0x76: BIT(6, mmu.read!byte(doubleReg(GPRegister.H, GPRegister.L)))            ; cycleTimer = 16; break;
                            case 0x77: BIT(6, GPReg[GPRegister.A])                                             ; cycleTimer = 8 ; break;
                            case 0x78: BIT(7, GPReg[GPRegister.B])                                             ; cycleTimer = 8 ; break;
                            case 0x79: BIT(7, GPReg[GPRegister.C])                                             ; cycleTimer = 8 ; break;
                            case 0x7A: BIT(7, GPReg[GPRegister.D])                                             ; cycleTimer = 8 ; break;
                            case 0x7B: BIT(7, GPReg[GPRegister.E])                                             ; cycleTimer = 8 ; break;
                            case 0x7C: BIT(7, GPReg[GPRegister.H])                                             ; cycleTimer = 8 ; break;
                            case 0x7D: BIT(7, GPReg[GPRegister.L])                                             ; cycleTimer = 8 ; break;
                            case 0x7E: BIT(7, mmu.read!byte(doubleReg(GPRegister.H, GPRegister.L)))            ; cycleTimer = 16; break;
                            case 0x7F: BIT(7, GPReg[GPRegister.A])                                             ; cycleTimer = 8 ; break;
                            case 0x80: RES(0, GPReg[GPRegister.B])                                             ; cycleTimer = 8 ; break;
                            case 0x81: RES(0, GPReg[GPRegister.C])                                             ; cycleTimer = 8 ; break;
                            case 0x82: RES(0, GPReg[GPRegister.D])                                             ; cycleTimer = 8 ; break;
                            case 0x83: RES(0, GPReg[GPRegister.E])                                             ; cycleTimer = 8 ; break;
                            case 0x84: RES(0, GPReg[GPRegister.H])                                             ; cycleTimer = 8 ; break;
                            case 0x85: RES(0, GPReg[GPRegister.L])                                             ; cycleTimer = 8 ; break;
                            case 0x86: RES(0, GPRegister.H, GPRegister.L)                                      ; cycleTimer = 16; break;
                            case 0x87: RES(0, GPReg[GPRegister.A])                                             ; cycleTimer = 8 ; break;
                            case 0x88: RES(1, GPReg[GPRegister.B])                                             ; cycleTimer = 8 ; break;
                            case 0x89: RES(1, GPReg[GPRegister.C])                                             ; cycleTimer = 8 ; break;
                            case 0x8A: RES(1, GPReg[GPRegister.D])                                             ; cycleTimer = 8 ; break;
                            case 0x8B: RES(1, GPReg[GPRegister.E])                                             ; cycleTimer = 8 ; break;
                            case 0x8C: RES(1, GPReg[GPRegister.H])                                             ; cycleTimer = 8 ; break;
                            case 0x8D: RES(1, GPReg[GPRegister.L])                                             ; cycleTimer = 8 ; break;
                            case 0x8E: RES(1, GPRegister.H, GPRegister.L)                                      ; cycleTimer = 16; break;
                            case 0x8F: RES(1, GPReg[GPRegister.A])                                             ; cycleTimer = 8 ; break;
                            case 0x90: RES(2, GPReg[GPRegister.B])                                             ; cycleTimer = 8 ; break;
                            case 0x91: RES(2, GPReg[GPRegister.C])                                             ; cycleTimer = 8 ; break;
                            case 0x92: RES(2, GPReg[GPRegister.D])                                             ; cycleTimer = 8 ; break;
                            case 0x93: RES(2, GPReg[GPRegister.E])                                             ; cycleTimer = 8 ; break;
                            case 0x94: RES(2, GPReg[GPRegister.H])                                             ; cycleTimer = 8 ; break;
                            case 0x95: RES(2, GPReg[GPRegister.L])                                             ; cycleTimer = 8 ; break;
                            case 0x96: RES(2, GPRegister.H, GPRegister.L)                                      ; cycleTimer = 16; break;
                            case 0x97: RES(2, GPReg[GPRegister.A])                                             ; cycleTimer = 8 ; break;
                            case 0x98: RES(3, GPReg[GPRegister.B])                                             ; cycleTimer = 8 ; break;
                            case 0x99: RES(3, GPReg[GPRegister.C])                                             ; cycleTimer = 8 ; break;
                            case 0x9A: RES(3, GPReg[GPRegister.D])                                             ; cycleTimer = 8 ; break;
                            case 0x9B: RES(3, GPReg[GPRegister.E])                                             ; cycleTimer = 8 ; break;
                            case 0x9C: RES(3, GPReg[GPRegister.H])                                             ; cycleTimer = 8 ; break;
                            case 0x9D: RES(3, GPReg[GPRegister.L])                                             ; cycleTimer = 8 ; break;
                            case 0x9E: RES(3, GPRegister.H, GPRegister.L)                                      ; cycleTimer = 16; break;
                            case 0x9F: RES(3, GPReg[GPRegister.A])                                             ; cycleTimer = 8 ; break;
                            case 0xA0: RES(4, GPReg[GPRegister.B])                                             ; cycleTimer = 8 ; break;
                            case 0xA1: RES(4, GPReg[GPRegister.C])                                             ; cycleTimer = 8 ; break;
                            case 0xA2: RES(4, GPReg[GPRegister.D])                                             ; cycleTimer = 8 ; break;
                            case 0xA3: RES(4, GPReg[GPRegister.E])                                             ; cycleTimer = 8 ; break;
                            case 0xA4: RES(4, GPReg[GPRegister.H])                                             ; cycleTimer = 8 ; break;
                            case 0xA5: RES(4, GPReg[GPRegister.L])                                             ; cycleTimer = 8 ; break;
                            case 0xA6: RES(4, GPRegister.H, GPRegister.L)                                      ; cycleTimer = 16; break;
                            case 0xA7: RES(4, GPReg[GPRegister.A])                                             ; cycleTimer = 8 ; break;
                            case 0xA8: RES(5, GPReg[GPRegister.B])                                             ; cycleTimer = 8 ; break;
                            case 0xA9: RES(5, GPReg[GPRegister.C])                                             ; cycleTimer = 8 ; break;
                            case 0xAA: RES(5, GPReg[GPRegister.D])                                             ; cycleTimer = 8 ; break;
                            case 0xAB: RES(5, GPReg[GPRegister.E])                                             ; cycleTimer = 8 ; break;
                            case 0xAC: RES(5, GPReg[GPRegister.H])                                             ; cycleTimer = 8 ; break;
                            case 0xAD: RES(5, GPReg[GPRegister.L])                                             ; cycleTimer = 8 ; break;
                            case 0xAE: RES(5, GPRegister.H, GPRegister.L)                                      ; cycleTimer = 16; break;
                            case 0xAF: RES(5, GPReg[GPRegister.A])                                             ; cycleTimer = 8 ; break;
                            case 0xB0: RES(6, GPReg[GPRegister.B])                                             ; cycleTimer = 8 ; break;
                            case 0xB1: RES(6, GPReg[GPRegister.C])                                             ; cycleTimer = 8 ; break;
                            case 0xB2: RES(6, GPReg[GPRegister.D])                                             ; cycleTimer = 8 ; break;
                            case 0xB3: RES(6, GPReg[GPRegister.E])                                             ; cycleTimer = 8 ; break;
                            case 0xB4: RES(6, GPReg[GPRegister.H])                                             ; cycleTimer = 8 ; break;
                            case 0xB5: RES(6, GPReg[GPRegister.L])                                             ; cycleTimer = 8 ; break;
                            case 0xB6: RES(6, GPRegister.H, GPRegister.L)                                      ; cycleTimer = 16; break;
                            case 0xB7: RES(6, GPReg[GPRegister.A])                                             ; cycleTimer = 8 ; break;
                            case 0xB8: RES(7, GPReg[GPRegister.B])                                             ; cycleTimer = 8 ; break;
                            case 0xB9: RES(7, GPReg[GPRegister.C])                                             ; cycleTimer = 8 ; break;
                            case 0xBA: RES(7, GPReg[GPRegister.D])                                             ; cycleTimer = 8 ; break;
                            case 0xBB: RES(7, GPReg[GPRegister.E])                                             ; cycleTimer = 8 ; break;
                            case 0xBC: RES(7, GPReg[GPRegister.H])                                             ; cycleTimer = 8 ; break;
                            case 0xBD: RES(7, GPReg[GPRegister.L])                                             ; cycleTimer = 8 ; break;
                            case 0xBE: RES(7, GPRegister.H, GPRegister.L)                                      ; cycleTimer = 16; break;
                            case 0xBF: RES(7, GPReg[GPRegister.A])                                             ; cycleTimer = 8 ; break;
                            case 0xC0: GPReg[GPRegister.B] |= 0x01                                             ; cycleTimer = 8 ; break;
                            case 0xC1: GPReg[GPRegister.C] |= 0x01                                             ; cycleTimer = 8 ; break;
                            case 0xC2: GPReg[GPRegister.D] |= 0x01                                             ; cycleTimer = 8 ; break;
                            case 0xC3: GPReg[GPRegister.E] |= 0x01                                             ; cycleTimer = 8 ; break;
                            case 0xC4: GPReg[GPRegister.H] |= 0x01                                             ; cycleTimer = 8 ; break;
                            case 0xC5: GPReg[GPRegister.L] |= 0x01                                             ; cycleTimer = 8 ; break;
                            case 0xC6: auto address = doubleReg(GPRegister.H, GPRegister.L); 
                                       mmu.write!ubyte(address, mmu.read!byte(address) | 0x01)                 ; cycleTimer = 16; break;
                            case 0xC7: GPReg[GPRegister.A] |= 0x01                                             ; cycleTimer = 8 ; break;
                            case 0xC8: GPReg[GPRegister.B] |= 0x02                                             ; cycleTimer = 8 ; break;
                            case 0xC9: GPReg[GPRegister.C] |= 0x02                                             ; cycleTimer = 8 ; break;
                            case 0xCA: GPReg[GPRegister.D] |= 0x02                                             ; cycleTimer = 8 ; break;
                            case 0xCB: GPReg[GPRegister.E] |= 0x02                                             ; cycleTimer = 8 ; break;
                            case 0xCC: GPReg[GPRegister.H] |= 0x02                                             ; cycleTimer = 8 ; break;
                            case 0xCD: GPReg[GPRegister.L] |= 0x02                                             ; cycleTimer = 8 ; break;
                            case 0xCE: auto address = doubleReg(GPRegister.H, GPRegister.L); 
                                       mmu.write!ubyte(address, mmu.read!byte(address) | 0x02)                 ; cycleTimer = 16; break;
                            case 0xCF: GPReg[GPRegister.A] |= 0x02                                             ; cycleTimer = 8 ; break;
                            case 0xD0: GPReg[GPRegister.B] |= 0x04                                             ; cycleTimer = 8 ; break;
                            case 0xD1: GPReg[GPRegister.C] |= 0x04                                             ; cycleTimer = 8 ; break;
                            case 0xD2: GPReg[GPRegister.D] |= 0x04                                             ; cycleTimer = 8 ; break;
                            case 0xD3: GPReg[GPRegister.E] |= 0x04                                             ; cycleTimer = 8 ; break;
                            case 0xD4: GPReg[GPRegister.H] |= 0x04                                             ; cycleTimer = 8 ; break;
                            case 0xD5: GPReg[GPRegister.L] |= 0x04                                             ; cycleTimer = 8 ; break;
                            case 0xD6: auto address = doubleReg(GPRegister.H, GPRegister.L); 
                                       mmu.write!ubyte(address, mmu.read!byte(address) | 0x04)                 ; cycleTimer = 16; break;
                            case 0xD7: GPReg[GPRegister.A] |= 0x04                                             ; cycleTimer = 8 ; break;
                            case 0xD8: GPReg[GPRegister.B] |= 0x08                                             ; cycleTimer = 8 ; break;
                            case 0xD9: GPReg[GPRegister.C] |= 0x08                                             ; cycleTimer = 8 ; break;
                            case 0xDA: GPReg[GPRegister.D] |= 0x08                                             ; cycleTimer = 8 ; break;
                            case 0xDB: GPReg[GPRegister.E] |= 0x08                                             ; cycleTimer = 8 ; break;
                            case 0xDC: GPReg[GPRegister.H] |= 0x08                                             ; cycleTimer = 8 ; break;
                            case 0xDD: GPReg[GPRegister.L] |= 0x08                                             ; cycleTimer = 8 ; break;
                            case 0xDE: auto address = doubleReg(GPRegister.H, GPRegister.L); 
                                       mmu.write!ubyte(address, mmu.read!byte(address) | 0x08)                 ; cycleTimer = 16; break;
                            case 0xDF: GPReg[GPRegister.A] |= 0x08                                             ; cycleTimer = 8 ; break;
                            case 0xE0: GPReg[GPRegister.B] |= 0x10                                             ; cycleTimer = 8 ; break;
                            case 0xE1: GPReg[GPRegister.C] |= 0x10                                             ; cycleTimer = 8 ; break;
                            case 0xE2: GPReg[GPRegister.D] |= 0x10                                             ; cycleTimer = 8 ; break;
                            case 0xE3: GPReg[GPRegister.E] |= 0x10                                             ; cycleTimer = 8 ; break;
                            case 0xE4: GPReg[GPRegister.H] |= 0x10                                             ; cycleTimer = 8 ; break;
                            case 0xE5: GPReg[GPRegister.L] |= 0x10                                             ; cycleTimer = 8 ; break;
                            case 0xE6: auto address = doubleReg(GPRegister.H, GPRegister.L); 
                                       mmu.write!ubyte(address, mmu.read!byte(address) | 0x10)                 ; cycleTimer = 16; break;
                            case 0xE7: GPReg[GPRegister.A] |= 0x10                                             ; cycleTimer = 8 ; break;
                            case 0xE8: GPReg[GPRegister.B] |= 0x20                                             ; cycleTimer = 8 ; break;
                            case 0xE9: GPReg[GPRegister.C] |= 0x20                                             ; cycleTimer = 8 ; break;
                            case 0xEA: GPReg[GPRegister.D] |= 0x20                                             ; cycleTimer = 8 ; break;
                            case 0xEB: GPReg[GPRegister.E] |= 0x20                                             ; cycleTimer = 8 ; break;
                            case 0xEC: GPReg[GPRegister.H] |= 0x20                                             ; cycleTimer = 8 ; break;
                            case 0xED: GPReg[GPRegister.L] |= 0x20                                             ; cycleTimer = 8 ; break;
                            case 0xEE: auto address = doubleReg(GPRegister.H, GPRegister.L); 
                                       mmu.write!ubyte(address, mmu.read!byte(address) | 0x20)                 ; cycleTimer = 16; break;
                            case 0xEF: GPReg[GPRegister.A] |= 0x20                                             ; cycleTimer = 8 ; break;
                            case 0xF0: GPReg[GPRegister.B] |= 0x40                                             ; cycleTimer = 8 ; break;
                            case 0xF1: GPReg[GPRegister.C] |= 0x40                                             ; cycleTimer = 8 ; break;
                            case 0xF2: GPReg[GPRegister.D] |= 0x40                                             ; cycleTimer = 8 ; break;
                            case 0xF3: GPReg[GPRegister.E] |= 0x40                                             ; cycleTimer = 8 ; break;
                            case 0xF4: GPReg[GPRegister.H] |= 0x40                                             ; cycleTimer = 8 ; break;
                            case 0xF5: GPReg[GPRegister.L] |= 0x40                                             ; cycleTimer = 8 ; break;
                            case 0xF6: auto address = doubleReg(GPRegister.H, GPRegister.L); 
                                       mmu.write!ubyte(address, mmu.read!byte(address)| 0x40)                  ; cycleTimer = 16; break;
                            case 0xF7: GPReg[GPRegister.A] |= 0x40                                             ; cycleTimer = 8 ; break;
                            case 0xF8: GPReg[GPRegister.B] |= 0x80                                             ; cycleTimer = 8 ; break;
                            case 0xF9: GPReg[GPRegister.C] |= 0x80                                             ; cycleTimer = 8 ; break;
                            case 0xFA: GPReg[GPRegister.D] |= 0x80                                             ; cycleTimer = 8 ; break;
                            case 0xFB: GPReg[GPRegister.E] |= 0x80                                             ; cycleTimer = 8 ; break;
                            case 0xFC: GPReg[GPRegister.H] |= 0x80                                             ; cycleTimer = 8 ; break;
                            case 0xFD: GPReg[GPRegister.L] |= 0x80                                             ; cycleTimer = 8 ; break;
                            case 0xFE: auto address = doubleReg(GPRegister.H, GPRegister.L); 
                                       mmu.write!ubyte(address, mmu.read!byte(address) | 0x80)                 ; cycleTimer = 16; break;
                            case 0xFF: GPReg[GPRegister.A] |= 0x80                                             ; cycleTimer = 8 ; break;
                            default  : unimplementedInstruction(op, mmu.read!ubyte(cast(ushort)(PC-1)));
                        } 
                        break;
                    case 0xCC: CALL(cast(bool)(GPReg[GPRegister.F] & FFlag.Z))                           ; cycleTimer = 12; break;
                    case 0xCD: CALL                                                                      ; cycleTimer = 12; break;
                    case 0xCE: ADDC(mmu.read!byte(PC++))                                                 ; cycleTimer = 8 ; break;
                    case 0xCF: CALL(0x08)                                                                ; cycleTimer = 32; break;
                    case 0xD0: RET(!(GPReg[GPRegister.F] & FFlag.C))                                     ; cycleTimer = 8 ; break;
                    case 0xD1: POP(GPRegister.D, GPRegister.E)                                           ; cycleTimer = 12; break;
                    case 0xD2: JP(!(GPReg[GPRegister.F] & FFlag.C))                                      ; cycleTimer = 12; break;
                    case 0xD4: CALL(!cast(bool)(GPReg[GPRegister.F] & FFlag.C))                          ; cycleTimer = 12; break;
                    case 0xD5: PUSH(GPRegister.D, GPRegister.E)                                          ; cycleTimer = 16; break;
                    case 0xD6: SUB(mmu.read!byte(PC++))                                                  ; cycleTimer = 8 ; break;
                    case 0xD7: CALL(0x10)                                                                ; cycleTimer = 32; break;
                    case 0xD8: RET(cast(bool)(GPReg[GPRegister.F] & FFlag.C))                            ; cycleTimer = 8 ; break;
                    case 0xD9: RET; interruptsEnabled = true                                             ; cycleTimer = 8 ; break;
                    case 0xDA: JP(cast(bool)(GPReg[GPRegister.F] & FFlag.C))                             ; cycleTimer = 12; break;
                    case 0xDC: CALL(cast(bool)(GPReg[GPRegister.F] & FFlag.C))                           ; cycleTimer = 12; break;
                    case 0xDE: SUBC(mmu.read!byte(PC++))                                                 ; cycleTimer = 8 ; break;
                    case 0xDF: CALL(0x18)                                                                ; cycleTimer = 32; break;
                    case 0xE0: mmu.write(0xFF00+mmu.read!ubyte(PC++), GPReg[GPRegister.A])               ; cycleTimer = 12; break;
                    case 0xE1: POP(GPRegister.H, GPRegister.L)                                           ; cycleTimer = 12; break;
                    case 0xE2: mmu.write(0xFF00+cast(ubyte)GPReg[GPRegister.C], GPReg[GPRegister.A])     ; cycleTimer = 8 ; break;
                    case 0xE5: PUSH(GPRegister.H, GPRegister.L)                                          ; cycleTimer = 16; break;
                    case 0xE6: AND(mmu.read!byte(PC++))                                                  ; cycleTimer = 8 ; break;
                    case 0xE7: CALL(0x20)                                                                ; cycleTimer = 32; break;
                    case 0xE8: ADDSP                                                                     ; cycleTimer = 16; break;
                    case 0xE9: PC = doubleReg(GPRegister.H, GPRegister.L)                                ; cycleTimer = 4 ; break;
                    case 0xEA: mmu.write(mmu.read!ushort(PC), GPReg[GPRegister.A]); PC += 2              ; cycleTimer = 16; break;
                    case 0xEE: XOR(mmu.read!byte(PC++))                                                  ; cycleTimer = 8 ; break;
                    case 0xEF: CALL(0x28)                                                                ; cycleTimer = 32; break;
                    case 0xF0: GPReg[GPRegister.A] = mmu.read!byte(0xFF00+mmu.read!ubyte(PC++))          ; cycleTimer = 12; break;
                    case 0xF1: POP(GPRegister.A, GPRegister.F)                                           ; cycleTimer = 12; break;
                    case 0xF2: GPReg[GPRegister.A] = mmu.read!byte(0xFF00+cast(ubyte)GPReg[GPRegister.C]); cycleTimer = 8 ; break;
                    case 0xF3: disableInterrupts = true                                                  ; cycleTimer = 4 ; break;
                    case 0xF5: PUSH(GPRegister.A, GPRegister.F)                                          ; cycleTimer = 16; break;
                    case 0xF6: OR(mmu.read!byte(PC++))                                                   ; cycleTimer = 8 ; break;
                    case 0xF7: CALL(0x30)                                                                ; cycleTimer = 32; break;
                    case 0xF8: LDHL                                                                      ; cycleTimer = 12; break;
                    case 0xF9: SP = doubleReg(GPRegister.H, GPRegister.L)                                ; cycleTimer = 8 ; break;
                    case 0xFA: GPReg[GPRegister.A] = mmu.read!byte(mmu.read!ushort(PC)); PC += 2         ; cycleTimer = 16; break;
                    case 0xFB: enableInterrupts = true                                                   ; cycleTimer = 4 ; break;
                    case 0xFE: CP(mmu.read!byte(PC++))                                                   ; cycleTimer = 8 ; break;
                    case 0xFF: CALL(0x38)                                                                ; cycleTimer = 32; break;
                    default  : unimplementedInstruction(op);
                }
                
                //Check if interrupts should be disabled
                if (op != 0xF3 && disableInterrupts) {
                    interruptsEnabled = false;
                    disableInterrupts = false;
                }
                if (op != 0xFB && enableInterrupts) {
                    interruptsEnabled = true;
                    enableInterrupts  = false;
                }

                //Update variables
                ++instCount;
                if (debugMode && breakOP == op) {
                    breakOP = -2;
                }
            }

            //Handle interrupts
            if (mmu.read!ubyte(ExtraReg.IFLAGS) & mmu.read!ubyte(ExtraReg.IEFlags)) {
                //Unhalt the CPU in the event that any interrupt flag is set, even if the interrupt doesn't occur
                halted = false;
            }
            testInterrupt(0x01, 0x0040); //VBLANK interrupt
            testInterrupt(0x02, 0x0048); //LCDSTAT interrupt
            testInterrupt(0x04, 0x0050); //Timer interrupt
            testInterrupt(0x10, 0x0060); //Key press interrupt

            //Update variables
            mmu.updateTimer(time);

            //Print debugging information
            if (debugMode && !halted) {
                if (debugOutput) {
                    printDebug;
                }

                //Show the debugging prompt if debugging mode is enabled and no breakpoint is being held
                if ((breakPC == 0 && breakOP == -1) || breakPC == PC || breakOP < -1) {
                    breakPC = 0;
                    breakOP = -1;

                    bool stop;
                    while (!stop) {
                        //Repeat the prompt if an invalid command was given
                        write("> ");
                        string cmd = readln;
                        string[] tokens = cmd.split;
                        if (!tokens.length) {
                            if (prevCmd) {
                                tokens = prevCmd.split;
                            } else {
                                writeln("Please enter a command");
                                continue;
                            }
                        } else {
                            prevCmd = cmd;
                        }                            

                        switch (tokens[0]) {
                            case "skip":     
                                if (tokens.length > 1) {
                                    formattedRead(tokens[1], "%x", &breakPC);
                                } else {
                                    breakPC = 0xFFFF;
                                }
                                stop = true;
                                break;
                            case "next":
                                stop = true;
                                break;
                            case "op":
                                formattedRead(tokens[1], "%x", &breakOP);
                                stop = true;
                                break;
                            case "nolog":
                                debugOutput = false;
                                break;
                            case "log":
                                debugOutput = true;
                                break;
                            default:     
                                writeln("Invalid command \"", tokens[0], "\"");
                        }
                    }
                }
            }
            
            //Return the number of nanoseconds emulated
            return cast(ulong)((1e9/clockSpeed)*cycleTimer);
        }
}

