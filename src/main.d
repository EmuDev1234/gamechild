import gb;
import dbg;
import memedit;
import std.getopt,
       std.datetime.stopwatch;
import core.thread;
import bindbc.sdl;
import gtk.Main,
       gtk.MainWindow,
       gtk.FileChooserDialog,
       gtk.MessageDialog;

pragma(lib, "gtkd-3");
pragma(lib, "SDL2");
pragma(lib, "BindBC_SDL");

void main(string[] args) {
    //Handle command line options
    bool[4] dbgOptions;
    auto gameboyType = GBType.Auto;
    bool unlimitedSpeed;
    bool memEditor;
    bool help;
    getopt(args, "cpu-debug|c", &dbgOptions[0], 
                 "gpu-debug|g", &dbgOptions[1],
                 "apu-debug|a", &dbgOptions[2], 
                 "mmu-debug|m", &dbgOptions[3],
                 "type"       , &gameboyType,
                 "maxspeed"   , &unlimitedSpeed,
                 "memedit"    , &memEditor,
                 "help"       , &help);

    //Get the rom path from the environment arguments if possible
    string rom;
    foreach (i,e; args) {
        if (i != 0 && e[0] != '-') {
            rom = e;
            break;
        }
    }

    //Initialise GTK and open a file choosing dialog if a rom path couldn't be found
    Main.init(args);
    if (!rom) {
        auto dialog = new FileChooserDialog("Select a ROM file", null, GtkFileChooserAction.OPEN);
        dialog.run;
        rom = dialog.getFilename;
        dialog.close;
        while (Main.eventsPending) {
            Main.iteration;
        }
    }

    //Main loop
    GB gameboy;
    MemEditor memEditorWin;
    if (memEditor) {
        memEditorWin = new MemEditor(gameboy);
        while (!memEditorWin.isOpened) {
            Main.iteration;
        }
    }

    try {
        //Load a game and start the timer
        gameboy.loadGame(rom, cast(Debug)((dbgOptions[0] ? Debug.CPU : 0) | (dbgOptions[1] ? Debug.GPU : 0) | (dbgOptions[2] ? Debug.APU : 0) | (dbgOptions[3] ? Debug.MMU : 0)), gameboyType);
        StopWatch frameTimer;
        frameTimer.start;

        while (gameboy.power) {
            //Update GTK events for memory editor window
            while (memEditor && Main.eventsPending) {
                Main.iteration;
            }

            //Handle SDL event loop
            SDL_Event event;
            while (SDL_PollEvent(&event)) {
                switch (event.type) {
                    case SDL_QUIT:
                        return;
                    case SDL_KEYDOWN:
                        gameboy.keyEvent(event.key.keysym.sym);
                        break;
                    case SDL_KEYUP:
                        gameboy.keyEvent(event.key.keysym.sym, false);
                        break;
                    default:
                        break;
                }
            }

            //Update the gameboy emulation and sleep
            gameboy.update;
            auto timePassed = frameTimer.peek.total!"nsecs";
            if (!unlimitedSpeed && timePassed < 1e9/59.7) {
                Thread.sleep(dur!"nsecs"(cast(ulong)(1e9/59.7 - timePassed)));
            }        
            frameTimer.reset; 
        }
    } catch (Throwable exception) {
        //Close all windows and create an error message dialog if an exception occurs
        if (memEditor) {
            memEditorWin.close;
            while (Main.eventsPending) {
                Main.iteration;
            }
        }
        gameboy.destroy;

        new MessageDialog(null, cast(GtkDialogFlags)0, GtkMessageType.ERROR, GtkButtonsType.CLOSE, "%s", exception.msg).run;
        throw exception;
    }
}
