import mmu;
import gb;
import std.conv,
       std.format,
       std.typecons;
import cairo.Context;
import gtk.Main,
       gtk.Widget,
       gtk.Window,
       gtk.Box,
       gtk.Grid,
       gtk.Label,
       gtk.Button,
       gtk.RadioButton,
       gtk.TreeIter,
       gtk.TreePath,
       gtk.TreeView,
       gtk.TreeViewColumn,
       gtk.ListStore,
       gtk.CellRendererText,
       gtk.CellRenderer,
       gtk.Entry;

class MemEditor : Window {
    final private:
        bool searched;
        bool active;
        GB* gameboy;
        ushort[] searchAddresses;
        ubyte compareValue;
        enum SearchMode {
            Exact, Different, Lesser, Greater
        }

        SearchMode mode;
        Box box;
        Grid grid;
        Entry searchEntry;
        RadioButton[4] searchSelect;
        Label searchLabel;
        Button resetButton;
        Button searchButton;
        TreeView searchView;
        ListStore searchList;

        bool setActive(Scoped!Context context, Widget window) {
            active = true;
            return false;
        }

        void setSearchMode(Button button) {
            //Find the button selected and then determine what mode was set
            foreach (i,e; searchSelect) {
                if (e is button) {
                    mode = cast(SearchMode)i;
                }
            }
        }

        bool match(ushort address) {
            //Compare a value in the Gameboy memory to the one set
            auto value = gameboy.memory.read!ubyte(address);
            final switch (mode) with (SearchMode) {
                case Exact:     return value == compareValue;
                case Different: return value != compareValue;
                case Lesser:    return value  < compareValue;
                case Greater:   return value  > compareValue;
            }
        }

        void edit(string path, string value, CellRendererText cellRenderer) {
            //Update the list entry and set the corresponding value in the Gameboys memory
            auto iter = new TreeIter(searchList, path);
            auto val  = to!ubyte(value);
            gameboy.memory.write(searchAddresses[to!uint(path)], val);
            searchList.setValue(new TreeIter(searchList, path), 1, val);
        }

        void reset(Button button) {
            searched = false;
            searchLabel.setText("New search begun");
            searchView.setModel(new ListStore([GType.STRING, GType.INT]));
            searchAddresses.length = 0;
        }

        void search(Button button) {
            //Get the exact value to compare against in exact mode, or use an older value in the differing modes
            if (mode == SearchMode.Exact || !searchAddresses.length) {
                compareValue = to!ubyte(searchEntry.getText);
            }

            //If there is nothing left to search then don't do anything
            if (searched && !searchAddresses.length) {
                searchLabel.setText("Nothing left to search!");
                return;
            }

            //Search either the entire address space of the gameboy, or the collection of addresses from the last search, to find matching values
            if (searched) {
                ushort[] newSearchAddresses;
                foreach (i; searchAddresses) {
                    if (match(i)) {
                        newSearchAddresses ~= i;
                    }
                }
                searchAddresses = newSearchAddresses;
            } else {
                foreach (ushort i; 0x0000..0xFFFF) {
                    if (match(i)) {
                        searchAddresses ~= i;
                    }
                }
            }

            //Show the results in the window
            if (searchAddresses.length <= 15) {
                searchList = new ListStore([GType.STRING, GType.INT]);
                foreach (i; searchAddresses) {
                    auto row = searchList.createIter;
                    searchList.setValue(row, 0, format("%04X", i));
                    searchList.setValue(row, 1, gameboy.memory.read!ubyte(i));
                }
                searchView.setModel(searchList);
            }
            searchLabel.setText(to!string(searchAddresses.length) ~ " results found");
            searched = true;
        }

    final public:
        bool isOpened() {
            return active;
        }
        
        void open(ref GB gameboy) {
            //Create a memory editor window bound to a gameboy value
            this.gameboy = &gameboy;
            box          = new Box(GtkOrientation.VERTICAL, 0);
            grid         = new Grid;
            addOnDraw(&setActive);
            setSkipTaskbarHint(true);
            setResizable(false);
            
            //Create the tree view for the search results
            searchView   = new TreeView;
            auto col = new TreeViewColumn;
            auto colRenderer = new CellRendererText;
            col.setTitle("Address");
            col.packStart(colRenderer, true);
            col.addAttribute(colRenderer, "text", 0);
            col.addAttribute(colRenderer, "visible", 0);
            searchView.appendColumn(col);

            col = new TreeViewColumn;
            colRenderer = new CellRendererText;
            col.setTitle("Value");
            col.packStart(colRenderer, true);
            col.addAttribute(colRenderer, "text", 1);
            col.addAttribute(colRenderer, "visible", 1);
            colRenderer.setProperty("editable", 1);
            colRenderer.addOnEdited(&edit);
            searchView.appendColumn(col);

            searchView.setModel(new ListStore([GType.STRING, GType.INT]));

            //Configure the grid
            foreach (i; 0..8) {
                grid.insertRow(0);
            }
            foreach (i; 0..2) {
                grid.insertColumn(0);
            }

            searchEntry     = new Entry("", 3);
            searchSelect[0] = new RadioButton("Exact");
            searchSelect[1] = new RadioButton(searchSelect[0].getGroup, "Different");
            searchSelect[2] = new RadioButton(searchSelect[0].getGroup, "Lesser than");
            searchSelect[3] = new RadioButton(searchSelect[0].getGroup, "Greater than");
            foreach (i; searchSelect) {
                i.addOnClicked(&setSearchMode);
            }
            searchLabel  = new Label("");
            resetButton  = new Button("Reset");
            searchButton = new Button("Search");

            grid.attach(new Label("Please enter the value to search: "), 0, 0, 1, 1);
            grid.attach(searchEntry, 1, 0, 1, 1);
            foreach (i,e; searchSelect) {
                grid.attach(e, 0, cast(int)(1+i), 2, 1); 
            }
            grid.attach(searchLabel, 0, 6, 1, 1);
            grid.attach(resetButton, 1, 6, 1, 1);            
            grid.attach(searchButton, 1, 7, 1, 1);

            //Configure and show the window
            searchView.setSizeRequest(300, 300);
            box.packEnd(searchView, true, false, 0);
            box.packEnd(grid, true, false, 0);
            resetButton.addOnClicked(&reset);
            searchButton.addOnClicked(&search);
            add(box);
            showAll;
        }

        this(ref GB gameboy) {
            super("Memory Editor");
            open(gameboy);
        }
}
