import mmu;
import type;
import bindbc.sdl;
import std.exception,
       std.conv,
       std.random,
       std.stdio;

struct APU {
    private:
        bool initialised;
        bool debugMode;
        static size_t initCount;
        alias Sample = short;
        enum outputFreq = 98200;
        enum frameSize  = 1024;
        enum sampleMult = Sample.max/(4*0xF);

        MMU mmu;
        SDL_AudioDeviceID device;
        Sample[frameSize*2] submitBuf;
        ulong lastSubmitTime;

        void init() {
            //Deinitialise if already initialised
            if (initialised) {
                deinit;
            } else {
                initCount++;
            }
            initialised = true;

            //Initialise SDL and open an audio output device
            if (!SDL_WasInit(SDL_INIT_AUDIO)) {
                enforce(!SDL_InitSubSystem(SDL_INIT_AUDIO), "Failed to initialise SDL: " ~ to!string(SDL_GetError));
            }
            
            SDL_AudioSpec[2] spec;
            with (spec[0]) {
                static assert (!__traits(isUnsigned, Sample)     , "Sample type must be signed");
                static assert ((frameSize & (frameSize - 1)) == 0, "Audio sample frame size must be a power of 2");
                format   = AUDIO_S16SYS;
                samples  = frameSize;
                channels = 2;
                freq     = outputFreq;
            }
            device = SDL_OpenAudioDevice(null, 0, &spec[0], &spec[1], 0);
            enforce(device, "Failed to open audio output device: " ~ to!string(SDL_GetError));
            SDL_PauseAudioDevice(device, 0);
        }

        void deinit() {
            if (initialised) {
                //Close the audio output device
                SDL_CloseAudioDevice(device);

                //Deinitialise SDL
                if (!(--initCount)) {
                    SDL_QuitSubSystem(SDL_INIT_AUDIO);
                }
            }
        }

        void printDebug() {
            bool savedDebugMode = mmu.debugMode;
            mmu.debugMode = false;

            with (mmu) {
                writefln("APU : AC1(Sweep(%02x), Duty(%02x), Vol(%02x), Data(%02x %02x))", read!ubyte(ExtraReg.AC1Sweep), read!ubyte(ExtraReg.AC1Duty), read!ubyte(ExtraReg.AC1Vol), read!ubyte(ExtraReg.AC1HData), read!ubyte(ExtraReg.AC1LData));
                writefln("      AC2(Duty(%02x), Vol(%02x), Data(%02x %02x))", read!ubyte(ExtraReg.AC2Duty), read!ubyte(ExtraReg.AC2Vol), read!ubyte(ExtraReg.AC2HData), read!ubyte(ExtraReg.AC2LData));
                writefln("      AC3(Enable(%02x), Vol(%02x), Data(%02x %02x), Wave(%(%02x %)))", read!ubyte(ExtraReg.AC3Enable), read!ubyte(ExtraReg.AC3Vol), read!ubyte(ExtraReg.AC3HData), read!ubyte(ExtraReg.AC3LData), read!(ubyte[16])(ExtraReg.AC3Wave));
                writefln("      AC4(Vol(%02x), Freq(%02x))", read!ubyte(ExtraReg.AC4Vol), read!ubyte(ExtraReg.AC4Freq));
            }

            mmu.debugMode = savedDebugMode;
        }

        void copySubmitBuf(ref Sample[] source, bool leftChannel, bool rightChannel) {
            //Add some sample values from a source buffer to the submit buffer
            size_t copyLen = source.length > submitBuf.length ? submitBuf.length : source.length;
            if (leftChannel && rightChannel) {
                //Copy all at once, can be compiler optimised using SIMD instructions
                submitBuf[0..copyLen] += source[0..copyLen];
            } else {
                //Copy each sample individually, considering which channels to use
                foreach (i,e; source[0..copyLen]) {
                    if (i%2 ? rightChannel : leftChannel) {
                        submitBuf[i] += e;
                    }
                }
            }
            source = source[copyLen..$];
        }

        void volEnvelope(ulong time, ubyte volReg, ref ulong lastShift, ref ubyte vol, ref bool resetVol) {
            //If some shifts have occured and envelope is enabled, increase or decrease a volume
            if ((volReg & 0x7) && time-lastShift >= 1e9/64.0) {
                if (resetVol) {
                    //Set default volume
                    vol = volReg >>> 4;
                    lastShift = time;
                }

                //Increase or decrease the volume and make sure to enforce the 0-F boundary
                ubyte shiftsPassed = cast(ubyte)((time-lastShift) / (1e9/64.0));
                if (volReg & 0x8) {
                    if (vol+shiftsPassed > 0xF) {
                        vol = 0xF;
                    } else {
                        vol += shiftsPassed;
                    }
                } else {
                    if (vol-shiftsPassed < 0) {
                        vol = 0;
                    } else {
                        vol -= shiftsPassed;
                    }
                }
                lastShift += cast(ulong)((1e9/64.0)*shiftsPassed);
                resetVol = false;
            } else if (!(volReg & 0x7)) {
                resetVol = true;
            }
        }

        //Channel 1
        Sample[] ch1SubmitBuf;
        ulong lastCh1SweepShift;
        size_t ch1SweepFreq;
        ulong lastCh1VolShift;
        bool ch1VolReset = true;
        ubyte ch1Vol;
        ulong ch1BeginTime;

        void updateChannel1(ulong time) {
            //Obtain the frequency and wave duty (proportion of frequency width taken by lower part of square wave)
            size_t frequency = cast(size_t)( 131072 / (2048-(mmu.read!ubyte(ExtraReg.AC1LData) | ((mmu.read!ubyte(ExtraReg.AC1HData) & 0x7) << 8))) );
            size_t freqWidth = outputFreq / frequency;
            if (freqWidth == 0) {
                //If the frequency to use is greater than the output frequency, use the output frequency
                freqWidth = 1;
            }
            size_t waveDuty = freqWidth;
            final switch ((mmu.read!ubyte(ExtraReg.AC1Duty) >> 6) & 0x3) {
                case 0: waveDuty *= 0.125; break;
                case 1: waveDuty *= 0.250; break;
                case 2: waveDuty *= 0.500; break;
                case 3: waveDuty *= 0.750; break;
            }

            //Reset if bit 7 of the HData register is set to 1
            ubyte hData = mmu.read!ubyte(ExtraReg.AC1HData);
            if (hData & 0x80) {
                ch1BeginTime = time;
                ch1SweepFreq = frequency;
                mmu.write!ubyte(ExtraReg.AC1HData, hData & ~0x80);
                if (!(mmu.read!ubyte(ExtraReg.AC1Duty) & 0xF)) {
                    mmu.write!ubyte(ExtraReg.AC1Duty, (hData & 0xF0) | 0xF);
                }
                ch1Vol = mmu.read!ubyte(ExtraReg.AC1Vol) >>> 4;
            }

            //Update frequency and volume - if sweep is enabled, increase or decrease the frequency at fixed intervals called shifts
            ubyte sweep = mmu.read!ubyte(ExtraReg.AC1Sweep);
            ubyte sweepTime = (sweep >>> 4) & 0x7;
            if (sweepTime) {
                //Every shift update the internal frequency
                if (time-lastCh1SweepShift >= (1e9/128.0) * sweepTime) {
                    //Calculate number of shifts passed
                    ubyte shiftsPassed = cast(ubyte)((time-lastCh1SweepShift) / ((1e9/128.0)*sweepTime));
                    lastCh1SweepShift += shiftsPassed * (1e9/128.0) * sweepTime;

                    //Calculate an increased or decreased frequency
                    size_t newFreq;
                    if (sweep & 0x8) {
                        newFreq = cast(size_t)(ch1SweepFreq + (ch1SweepFreq >> shiftsPassed));
                    } else {
                        newFreq = cast(size_t)(ch1SweepFreq - (ch1SweepFreq >> shiftsPassed));
                    }

                    //Perform overflow check - disable channel 1 if the frequency is greater than 2047
                    if (newFreq > 2048) {
                        mmu.write!ubyte(ExtraReg.AC1Sweep, sweep & 0xF);
                    } else {
                        ch1SweepFreq = newFreq;
                    }

                    //Update frequency in MMIO registers
                    mmu.write!ubyte(ExtraReg.AC1LData, cast(ubyte)(ch1SweepFreq & 0xF));
                    mmu.write!ubyte(ExtraReg.AC1HData, (mmu.read!ubyte(ExtraReg.AC1HData) & 0xF8) | ((ch1SweepFreq >>> 8) & 0x7));
                }
            }

            volEnvelope(time, mmu.read!ubyte(ExtraReg.AC1Vol), lastCh1VolShift, ch1Vol, ch1VolReset);

            //Add a square wave to the output buffer
            if (ch1SubmitBuf.length < submitBuf.length && ch1Vol) {
                foreach (i; 0..(submitBuf.length-ch1SubmitBuf.length)/freqWidth + 1) {
                    ch1SubmitBuf.length += freqWidth;
                    ch1SubmitBuf[$-freqWidth .. ($+waveDuty)-freqWidth] = cast(Sample)( sampleMult*ch1Vol);
                    ch1SubmitBuf[($+waveDuty)-freqWidth .. $          ] = cast(Sample)(-sampleMult*ch1Vol);
                }
            }
            if (ch1SubmitBuf.length) {
                ubyte enabled = mmu.read!ubyte(ExtraReg.AudioSel);
                bool leftChannel  = cast(bool)(enabled & 0x10);
                bool rightChannel = cast(bool)(enabled & 0x01);
                copySubmitBuf(ch1SubmitBuf, leftChannel, rightChannel);
            }      
        }

        //Channel 2
        Sample[] ch2SubmitBuf;
        ulong lastCh2VolShift;
        bool ch2VolReset = true;
        ubyte ch2Vol;
        ulong ch2BeginTime;

        void updateChannel2(ulong time) {
            //Obtain the frequency and wave duty (proportion of frequency width taken by lower part of square wave)
            size_t frequency = cast(size_t)( 131072 / (2048-(mmu.read!ubyte(ExtraReg.AC2LData) | ((mmu.read!ubyte(ExtraReg.AC2HData) & 0x7) << 8))) );
            size_t freqWidth = outputFreq / frequency;
            if (freqWidth == 0) {
                //If the frequency to use is greater than the output frequency, use the output frequency
                freqWidth = 1;
            }
            size_t waveDuty = freqWidth;
            final switch ((mmu.read!ubyte(ExtraReg.AC2Duty) >> 6) & 0x3) {
                case 0: waveDuty *= 0.125; break;
                case 1: waveDuty *= 0.250; break;
                case 2: waveDuty *= 0.500; break;
                case 3: waveDuty *= 0.750; break;
            }

            //Reset if bit 7 of the HData register is set to 1
            ubyte hData = mmu.read!ubyte(ExtraReg.AC2HData);
            if (hData & 0x80) {
                ch2BeginTime = time;
                mmu.write!ubyte(ExtraReg.AC2HData, hData & ~0x80);
                if (!(mmu.read!ubyte(ExtraReg.AC2Duty) & 0xF)) {
                    mmu.write!ubyte(ExtraReg.AC2Duty, (hData & 0xF0) | 0xF);
                }
                ch2Vol = mmu.read!ubyte(ExtraReg.AC2Vol) >>> 4;
            }

            volEnvelope(time, mmu.read!ubyte(ExtraReg.AC2Vol), lastCh2VolShift, ch2Vol, ch2VolReset);

            //Add a square wave to the output buffer
            if (ch2SubmitBuf.length < submitBuf.length && ch2Vol) {
                foreach (i; 0..(submitBuf.length-ch2SubmitBuf.length)/freqWidth + 1) {
                    ch2SubmitBuf.length += freqWidth;
                    ch2SubmitBuf[$-freqWidth .. ($+waveDuty)-freqWidth] = cast(Sample)(-sampleMult*ch2Vol);
                    ch2SubmitBuf[($+waveDuty)-freqWidth .. $          ] = cast(Sample)( sampleMult*ch2Vol);
                }
            }
            if (ch2SubmitBuf.length) {
                ubyte enabled = mmu.read!ubyte(ExtraReg.AudioSel);
                bool leftChannel  = cast(bool)(enabled & 0x20);
                bool rightChannel = cast(bool)(enabled & 0x02);
                copySubmitBuf(ch2SubmitBuf, leftChannel, rightChannel);
            }      
        }

        //Channel 3
        size_t curCh3WaveIndex;
        size_t ch3SamplesLeft;

        void valCopySubmitBuf(Sample sample, size_t src, size_t dest, bool leftChannel, bool rightChannel) {
            //Add a single sample to some segment of the submit buffer
            if (leftChannel && rightChannel) {
                //Copy all at once, can be compiler optimised using SIMD instructions
                submitBuf[src..dest] += sample;
            } else {
                //Copy to each sample one by one, considering what channels must be used
                foreach (i; src..dest) {
                    if (i%2 ? rightChannel : leftChannel) {
                        submitBuf[i] += sample;
                    }
                }
            }
        }

        void updateChannel3(ulong time) {
            if (mmu.read!ubyte(ExtraReg.AC3Enable) & 0x80) {
                //Get the volume and frequency to use
                double volume;
                final switch ((mmu.read!ubyte(ExtraReg.AC3Vol) >> 5) & 0x3) {
                    case 0: volume = 0.00; break;
                    case 1: volume = 1.00; break;
                    case 2: volume = 0.50; break;
                    case 3: volume = 0.25; break;
                }

                if (volume > 0.0) {
                    size_t frequency = cast(size_t)( 65536/(2048-(mmu.read!ubyte(ExtraReg.AC3LData) | ((mmu.read!ubyte(ExtraReg.AC3HData) & 0x7) << 8))) );
                    size_t freqWidth = outputFreq / frequency;

                    ubyte enabled = mmu.read!ubyte(ExtraReg.AudioSel);
                    bool leftChannel  = cast(bool)(enabled & 0x40);
                    bool rightChannel = cast(bool)(enabled & 0x04);

                    //Convert and copy the wave data to the output buffer
                    ubyte[16] wave = mmu.read!(ubyte[16])(ExtraReg.AC3Wave);
                    size_t seek;

                    while (seek < submitBuf.length) {
                        //Convert the unsigned 4 bit samples to a signed type
                        Sample convertSample = (wave[curCh3WaveIndex/2] >> (!(curCh3WaveIndex%2))*4) & 0xF;
                        convertSample -= 8;
                        convertSample *= 2*sampleMult*volume;

                        if (ch3SamplesLeft) {
                            //Add the rest of a sample from last time an output buffer was pushed
                            if (ch3SamplesLeft < submitBuf.length) {
                                valCopySubmitBuf(convertSample, 0, ch3SamplesLeft, leftChannel, rightChannel);
                                seek += ch3SamplesLeft;
                                ch3SamplesLeft = 0;
                            } else {
                                valCopySubmitBuf(convertSample, 0, submitBuf.length, leftChannel, rightChannel);
                                ch3SamplesLeft -= submitBuf.length;
                                break;
                            }
                        } else if (seek+freqWidth >= submitBuf.length) {
                            //End the output buffer if it is filled, record the amount of the sample to add next time
                            valCopySubmitBuf(convertSample, seek, submitBuf.length, leftChannel, rightChannel);
                            ch3SamplesLeft = freqWidth - (submitBuf.length - seek);
                            break;
                        } else {
                            //Just add a sample and move on
                            valCopySubmitBuf(convertSample, seek, seek+freqWidth, leftChannel, rightChannel);
                            seek += freqWidth;
                        }    
                        curCh3WaveIndex = (curCh3WaveIndex+1) % 32;                
                    }
                }
            }
        }

        //Channel 4
        Sample[] ch4SubmitBuf;
        ulong lastCh4VolShift;
        bool ch4VolReset = true;
        ubyte ch4Vol;

        void updateChannel4(ulong time) {
            //Get the volume and frequency to use
            ubyte freqReg = mmu.read!ubyte(ExtraReg.AC4Freq);
            size_t frequency = cast(size_t)(524288 / ((freqReg & 0x7) == 0 ? 0.5 : (freqReg & 0x7)) / 2^^((freqReg >>> 4)+1));
            size_t freqWidth = outputFreq / frequency;
            if (freqWidth == 0) {
                //If the frequency of the noise is higher than the output frequency, then just use the output frequency
                freqWidth = 1;
            }

            volEnvelope(time, mmu.read!ubyte(ExtraReg.AC4Vol), lastCh4VolShift, ch4Vol, ch4VolReset);
            
            //Generate noise audio data and then add some to the output buffer
            if (ch4SubmitBuf.length < submitBuf.length && ch4Vol) {
                foreach (i; 0..(submitBuf.length-ch4SubmitBuf.length)/freqWidth + 1) {
                    ch4SubmitBuf.length += freqWidth;
                    ch4SubmitBuf[$-freqWidth .. $] = cast(Sample)([-1,1][uniform(0,2)] * sampleMult * ch4Vol);
                }
            }
            if (ch4SubmitBuf.length) {
                ubyte enabled = mmu.read!ubyte(ExtraReg.AudioSel);
                bool leftChannel  = cast(bool)(enabled & 0x80);
                bool rightChannel = cast(bool)(enabled & 0x08);
                copySubmitBuf(ch4SubmitBuf, leftChannel, rightChannel);
            }        
        }

    public:
        void reset(GBType type, bool dbg = false, MMU newMMU = null) {
            deinit;
            init;
            debugMode = dbg;
            if (newMMU) {
                mmu = newMMU;
            }
        }

        void update(ulong time) {
            //If the data on the queue is going to run out, prepare the output buffer and submit it to the SDL queue
            if (time-lastSubmitTime > 1e9*((submitBuf.length/2.01)/outputFreq)) {
                if (debugMode) {
                    printDebug;
                }

                //Check if sound is actually enabled
                if (mmu.read!ubyte(ExtraReg.AudioSet) & 0x80) {
                    submitBuf[] = 0;
                    updateChannel1(time);
                    updateChannel2(time);
                    updateChannel3(time);
                    updateChannel4(time);

                    //Apply global volume configuration and then submit
                    ubyte vol = mmu.read!ubyte(ExtraReg.AudioVol);
                    double volLeft  = ((vol >> 4) & 0x7) / 7.0;
                    double volRight = (vol & 0x7) / 7.0;
                    foreach (i, ref e; submitBuf) {
                        e *= i%2 ? volRight : volLeft;
                    }
                    enforce(!SDL_QueueAudio(device, &submitBuf, typeof(submitBuf).sizeof), "Failed to play audio: " ~ to!string(SDL_GetError));
                    lastSubmitTime = time;
                }
            }
        }

        ~this() {
            deinit;
        }
}
