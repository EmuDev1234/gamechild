import std.file, 
       std.stdio, 
       std.format, 
       std.exception;

struct Cartridge {
    private:
        //Cartridge data and bank selectors
        const(ubyte)[] data;
        ubyte[0x2000][] ramBanks;
        ushort romBankSelector;
        ushort ramBankSelector;
        
        string file;
        bool ramUsed;

        //Control registers
        bool ramEnabled;
        bool ramSelectMode;

        bool rtcSelected;
        ubyte rtcFreezeState;
        ubyte rtcRegSelect;        
        ubyte[5] rtcReg;
        ubyte[5] rtcRegFrozen;
        size_t lastRTCtime;
        size_t rtcQuota;

        ubyte MBC;

    public:
        bool ramBankUsable(ushort address) {
            if (!ramEnabled) {
                //RAM bank is not usable if RAM is disabled
                return false;
            }
            if (MBC != 3 && (ramBankSelector >= ramBanks.length || (MBC == 2 && address > 0xA1FF))) {
                //Check if the ram bank selector is invalid, or a memory address out of the range for MBC2 is used 
                return false;
            }
            return true;
        }

        ref T rawRead(T)(ushort address) {
            switch ((address >> 12) & 0xF) {
                case 0x0: .. case 0x3:
                    //ROM bank #0 (unswitchable)
                    return *cast(T*)(&data[address]);
                case 0x4: .. case 0x7:
                    //Switchable ROM bank #1
                    return *cast(T*)(&data[(address-0x4000)+(romBankSelector*0x4000)]);
                case 0xA: .. case 0xB:
                    //RAM bank #0, or an RTC register with MBC2 if selected
                    if (!ramBankUsable(address)) {
                        throw new Error(format("Unusable cartridge address 0x%04x accessed", address));
                    }                    
                        
                    if (MBC == 3 && rtcSelected) {
                        if (rtcFreezeState >= 2) {
                            return *cast(T*)&rtcRegFrozen[rtcRegSelect];
                        } else {
                            return *cast(T*)&rtcReg[rtcRegSelect];
                        }
                    } else {
                        ramUsed = true;
                        if (ramSelectMode) {
                            //In RAM selection mode, use a specific RAM bank
                            return *cast(T*)(&ramBanks[ramBankSelector][address-0xA000]);
                        } else {
                            //In ROM selection mode, always use RAM bank 0
                            return *cast(T*)(&ramBanks[0][address-0xA000]);
                        }
                    }
                default:
                    throw new Exception(format("Invalid memory address %04x for cartridge", address));
            }
        }

        void write(T)(ushort address, T value) {
            switch (MBC) {
                case 0:
                    if (address >= 0xA000 && address <= 0xBFFF) {
                        //RAM bank with no MBC
                        ramEnabled = true;
                        rawRead!T(address) = value;
                    }
                    break;
                case 1:
                    switch ((address >> 12) & 0xF) {
                        case 0x0: .. case 0x1:
                            //Enables RAM R/W if any byte with 0xA as it's lowest nibble is written, disables otherwise
                            ramEnabled = ((value & 0xF) == 0xA);
                            break;
                        case 0x2: .. case 0x3:
                            //Writes to the lowest 5 bits of the ROM bank selector
                            value &= 0x1F;
                            romBankSelector &= 0xE0;
                            if (value == 0) {
                                value = 1;
                            }
                            romBankSelector |= value;
                            break;
                        case 0x4: .. case 0x5:
                            //Specifies RAM bank or upper 2 bits of ROM bank depending on selection mode
                            value &= 0x3;
                            if (ramSelectMode) {
                                ramBankSelector = cast(ubyte)value;
                            } else {
                                romBankSelector &= 0x1F;
                                romBankSelector |= cast(ubyte)((value & 0x3) << 5);
                            }
                            break;
                        case 0x6: .. case 0x7:
                            //Set bank selection mode
                            ramSelectMode = cast(bool)(value & 0x1);
                            break;
                        default:
                            rawRead!T(address) = value;
                    }
                    break;
                case 2:
                    switch ((address >> 12) & 0xF) {
                        case 0x0: .. case 0x1:
                            //Enables/disables RAM R/W if the least significant bit of the upper address byte is set to 0
                            if (!(address & 0x0100)) {
                                ramEnabled = !ramEnabled;
                            }
                            break;
                        case 0x2: .. case 0x3:
                            //Sets ROM bank number if least significant bit of upper address byte is 1
                            if (address & 0x0100) {
                                if ((value & 0xFF) == 0) {
                                    value = 1;
                                }
                                romBankSelector = value & 0xFF;
                            }
                            break;
                        case 0xA:
                            //RAM built in to MBC2 - only use lowest 4 bits of each byte
                            if (ramEnabled && address <= 0xA1FF) {
                                T toWrite = value;
                                foreach (ref i; cast(ubyte[])(&toWrite)[0..T.sizeof]) {
                                    toWrite &= 0xF;
                                }
                                rawRead!T(address) = toWrite;
                            } else if (ramEnabled) {
                                goto default;
                            }
                            break;
                        default:
                            rawRead!T(address) = value;
                    }
                    break;
                case 3:
                    switch ((address >> 12) & 0xF) {
                        case 0x0: .. case 0x1:
                            //Enables RAM and RTC if value with 0xA as lower nibble is written, otherwise disables
                            ramEnabled = ((value & 0xF) == 0xA);
                            break;
                        case 0x2: .. case 0x3:
                            //Selects 7 bit ROM bank number
                            if ((value & 0x7F) == 0) {
                                value = 1;
                            }
                            romBankSelector = value & 0x7F;
                            break;
                        case 0x4: .. case 0x5:
                            //RAM bank number or RTC register select
                            value = value & 0xF;
                            if (value < 0x8) {
                                ramBankSelector = cast(ubyte)value;
                            } else if (value < 0xD) {
                                rtcSelected  = true;
                                rtcRegSelect = cast(ubyte)(value-0x7);
                            }
                            break;
                        case 0x6: .. case 0x7:
                            //Toggle timer read freeze if 0 and 1 are written in succession
                            if ((value & 0x1) && (rtcFreezeState == 1 || rtcFreezeState == 3)) {
                                rtcFreezeState = (rtcFreezeState+1)%4;
                                if (rtcFreezeState == 2) {
                                    rtcRegFrozen = rtcReg;
                                }
                            } else if (!(value & 0x1)) {
                                if (rtcFreezeState != 1) {
                                    rtcFreezeState = (rtcFreezeState+1)%4;
                                } else {
                                    rtcFreezeState = 0;
                                }
                            }
                            break;
                        case 0xA: .. case 0xB:
                            //RAM bank 0-3, or RTC register if selected
                            rawRead!T(address) = value;
                            break;
                        default:
                            rawRead!T(address) = value;
                    }
                    break;
                case 5:
                    switch ((address >> 12) & 0xF) {
                        case 0x0: .. case 0x1:
                            //Enables RAM R/W if any byte with 0xA as it's lowest nibble is written, disables otherwise
                            ramEnabled = ((value & 0xF) == 0xA);
                            break;
                        case 0x2:
                            //Lowest 8 bits of ROM bank number
                            romBankSelector &= 0xFF00;
                            romBankSelector |= value & 0xFF;
                            break;
                        case 0x3:
                            //Highest bit of ROM bank number
                            romBankSelector &= 0x00FF;
                            romBankSelector |= (value << 8) & 0x100;
                            break;
                        case 0x4: .. case 0x5:
                            //RAM bank number
                            ramBankSelector = (value & 0xF);
                            break;
                        default:
                            rawRead!T(address) = value;
                    }
                    break;
                default:
                    throw new Exception(format("Unimplemented MBC type %d for writing value", MBC));
            }
        }

        void updateRTC(size_t time) {
            if (MBC == 3 && !(rtcReg[4] & 0x40)) {
                //Update the RTC registers if the MBC is 3 and halting has been disabled
                rtcQuota += time-lastRTCtime;
                lastRTCtime = time;
                if (rtcQuota > 1e9) {
                    //Update seconds
                    rtcReg[0] = (rtcReg[0]+1)%60;
                    if (rtcReg[0] == 0) {
                        //Update minutes
                        rtcReg[1] = (rtcReg[1]+1)%60;
                        if (rtcReg[1] == 0) {
                            //Update hours
                            rtcReg[2] = (rtcReg[2]+1)%24;
                            if (rtcReg[2] == 0) {
                                //Update days
                                uint newDay = rtcReg[3] & ((rtcReg[4] & 0x1) << 8);
                                newDay = (newDay+1) & 0x1FF;
                                rtcReg[3] = newDay & 0xFF;
                                rtcReg[4] = cast(ubyte)((rtcReg[4] & ~0x01) | (0x01 * (newDay & 0x100)));
                                rtcReg[4] |= 0x80 * (newDay >= 512);
                            }
                        }
                    }

                    //Remove a second from counter
                    rtcQuota -= cast(size_t)1e9;
                }
            }
        }

        void loadSave() {
            //Load a RAM backup (save file)
            string saveFile = file ~ ".sav";
            if (saveFile.exists) {
                writeln("Loading save file \"", saveFile, "\"");
                File save = File(saveFile, "r");
                foreach (ref i; ramBanks) {
                    save.rawRead(i);
                }
            }
        }

        void storeSave() {
            //Backup RAM into a save file if it was used at any point
            if (ramUsed) {
                File save = File(file ~ ".sav", "w");
                foreach (i,e; ramBanks) {
                    save.rawWrite(e);
                }
            }
        }

        void rom(string file) {
            //Load the file ROM data
            this.file = file;
            ubyte[] newData;
            newData.length = getSize(file);
            File(file, "r").rawRead(newData);
            data = newData;

            //Configure the member variables based on memory bank controller
            MBC = data[0x147];
            romBankSelector = 1;
            ramBankSelector = 0;
            if (MBC == 0x00 || (MBC >= 0x08 && MBC <= 0x09)) {
                //No MBC
                MBC = 0;
            } else if (MBC >= 0x01 && MBC <= 0x03) {
                MBC = 1;
            } else if (MBC >= 0x05 && MBC <= 0x06) {
                MBC = 2;
            } else if (MBC >= 0x0F && MBC <= 0x13) {
                MBC = 3;
            } else if (MBC >= 0x19 && MBC <= 0x1E) {
                MBC = 5;
            } else if (MBC == 0x20) {
                MBC = 6;
            } else if (MBC == 0x22) {
                MBC = 7;
            } else {
                throw new Exception(format("Unrecognised cartridge type %02x", MBC));
            }
            
            //Configure RAM banks - always leave at least 1 to workaround invalid usage of the 0xA000-0xC000 memory region
            enforce(data[0x149] <= 0x05, format("Unrecognised memory bank number %02x", data[0x149]));
            ramBanks.length = [1, 1, 1, 4, 16, 8][data[0x149]];
            loadSave;
        }

        this(string file) { 
            rom = file; 
        }
}
