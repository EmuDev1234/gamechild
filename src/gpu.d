import mmu;
import dbg;
import type;
import bindbc.sdl;
import std.exception, 
       std.conv, 
       std.stdio;

struct GPU {
    private:
        //Main member variables
        bool initialised;
        static size_t initCount;

        MMU mmu;
        GBType type;
        SDL_Window* window;
        SDL_Surface* output;
        SDL_Renderer* renderer;
        ulong lcdTimer;
        ubyte lastLine;
        bool vblankSent;
        bool debugMode;

        enum screenW = 160;
        enum screenH = 144;

        void init() {
            //Deinitialise if already initialised
            if (initialised) {
                deinit;
            } else {
                initCount++;
            }
            initialised = true;

            //Initialise SDL and create a window
            if (!initCount) {
                enforce(!SDL_InitSubSystem(SDL_INIT_VIDEO), "Failed to initialise SDL: " ~ to!string(SDL_GetError()));
            }
            window   = SDL_CreateWindow("GameChild", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, screenW, screenH, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
            enforce(window, "Failed to create a window: " ~ to!string(SDL_GetError));
            renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
            enforce(renderer, "Failed to create a renderer: " ~ to!string(SDL_GetError));
            output   = SDL_CreateRGBSurface(0, screenW, screenH, 32, 0xFF000000, 0x00FF0000, 0x0000FF00, 0x000000FF);
            enforce(output, "Failed to create a renderer: " ~ to!string(SDL_GetError));
        }

        void deinit() {
            if (initialised) {
                //Destroy the window and deinitialise SDL
                SDL_FreeSurface(output);
                SDL_DestroyRenderer(renderer);
                SDL_DestroyWindow(window);
                if (!(--initCount)) {
                    SDL_QuitSubSystem(SDL_INIT_VIDEO);
                }
                initialised = false;
            }
        }

        void printDebug() {
            bool savedDebugMode = mmu.debugMode;
            mmu.debugMode = false;

            with (mmu) {
                writefln("GPU : LCDCONT(%02x), LCDStat(%02x), curScan(%d), scanComp(%d), xScroll(%d), yScroll(%d)", read!ubyte(ExtraReg.LCDCONT) , read!ubyte(ExtraReg.LCDStat), read!ubyte(ExtraReg.curScan),
                                                                                                                    read!ubyte(ExtraReg.scanComp), read!ubyte(ExtraReg.xScroll), read!ubyte(ExtraReg.yScroll));
            }

            mmu.debugMode = savedDebugMode;
        }

        void renderPatternLine(ubyte[] tileData, uint screenX, uint screenY, ubyte shades, bool xFlip = false, bool transparency = false) {
            //Render a tile/sprite pattern line to the screen
            foreach (x; 0..8) {
                auto flipIndex = xFlip ? 7-x : x;
                ubyte colIndex = ((tileData[0] >> 7-flipIndex) & 0x1) | ((flipIndex > 6 ? tileData[1] << 1 : tileData[1] >> 6-flipIndex) & 0x2);
                ubyte col = cast(ubyte)(0xFF - ((shades >> colIndex*2) & 0x3)*(0xFF/4));
                if ((!transparency || colIndex != 0) && screenX+x < screenW) {
                    (cast(uint*)output.pixels)[screenX + x + screenY*screenW] = (col << 24) | (col << 16) | (col << 8) | 0xFF;
                }
            }
        }

        /*void renderColorPatternLine(ubyte[] tileData, uint screenX, uint screenY, ubyte[2][] shades, bool xFlip = false, bool transparency = false) {
            //Render a gameboy color tile/sprite pattern line to the screen
            foreach (x; 0..8) {
                auto flipIndex = xFlip ? 7-x : x;
                ubyte colIndex = ((tileData[0] >> 7-flipIndex) & 0x1) | ((flipIndex > 6 ? tileData[1] << 1 : tileData[1] >> 6-flipIndex) & 0x2);
                if (!transparency || colIndex != 0) {
                    SDL_SetRenderDrawColor(renderer, shades[colIndex][0] & 0x1F, 
                                                    (shades[colIndex][0] >>> 5) | (shades[colIndex][1] & 0x3),
                                                    (shades[colIndex][1] >>> 2) & 0x1F, 0xFF);
                    SDL_RenderDrawPoint(renderer, screenX+x, screenY);
                }
            }
        }*/

        ubyte[] getTileLine(ubyte id, ubyte y) {
            ubyte LCDCONT = mmu.read!ubyte(ExtraReg.LCDCONT);

            //Obtain the pixel data for background and foreground tiles
            bool addressingMode = !cast(bool)(LCDCONT & 0x10);
            ushort tile = addressingMode ? 0x9000 : 0x8000;
            switch (addressingMode) {
                case 0:
                    tile = cast(ushort)(tile+(id*16));
                    break;
                case 1:
                    tile = cast(ushort)(tile+(cast(byte)(id)*16));
                    break;
                default:
                    break;
            }
            return mmu.rawRead!(ubyte[2])(cast(ushort)(tile+(y*2)));
        }

        void renderLine() {
            with (mmu) {
                //Render background tilemap if bit 0 of the LCDCont register is set
                ubyte LCDCONT = read!ubyte(ExtraReg.LCDCONT);
                ubyte[32][32] tiles;
                if (LCDCONT & 0x1) {
                    //Render a line of the background tile map
                    tiles = read!(ubyte[32][32])(LCDCONT & 0x08 ? 0x9C00 : 0x9800);
                    int posX = read!ubyte(ExtraReg.xScroll);
                    int posY = read!ubyte(ExtraReg.yScroll);
                    foreach (x; 0..32) {
                        //Render each tile in view
                        int tileX = (x*8) - (posX%8);
                        if (tileX > screenW) {
                            break;
                        }
                        if (tileX > -8) {
                            auto tileLine = getTileLine(tiles[((lastLine+posY)/8) % 32][(x+(posX/8)) % 32], cast(ubyte)((lastLine+posY) % 8));
                            if (this.type == GBType.GB) {
                                renderPatternLine(tileLine, tileX, lastLine, read!ubyte(ExtraReg.BGPalette));
                            } else {
                                //renderColorPatternLine(tileLine, tileX, lastLine, gbcPalette);
                            }
                        }
                    }
                }

                //Render sprites
                if (LCDCONT & 0x2) {
                    //Select the sprites to render
                    auto spriteAttrib = read!(ubyte[4][40])(0xFE00);
                    bool mode = cast(bool)(LCDCONT & 0x04);
                    size_t[40] toRender;
                    uint numRender;
                    foreach (i, ref e; spriteAttrib) {
                        //Only render a sprite if its x and y co-ordinates are not 0, and it is in the current scanline
                        int x = e[1]-8;
                        int y = e[0]-16;

                        auto yDist = lastLine-y;
                        if (!(x == -8 && y == -16) &&
                             (y <= lastLine) && ((mode && yDist < 16) || (yDist < 8))) {
                            toRender[numRender++] = i;
                        }
                    }

                    //Order the selected sprites: sprites lower down on the screen go below other sprites
                    foreach (i, ref e; toRender[0..numRender]) {
                        foreach (ref f; toRender[i+1..numRender]) {
                            if (spriteAttrib[f][0] < spriteAttrib[e][0]) {
                                size_t swap = e;
                                e = f;
                                f = swap;
                            }
                        }
                    }

                    //Render the sprites selected
                    foreach (i; toRender[0..numRender]) {
                        int x = spriteAttrib[i][1]-8;
                        int y = spriteAttrib[i][0]-16;
                        auto yDist = lastLine-y;

                        auto yIndex = spriteAttrib[i][2];
                        if (mode) {
                            if (yDist < 8) {
                                yIndex &= ~0x1;
                            } else {
                                yIndex |= 0x1;
                                yDist -= 8;
                            }
                        }
                        auto yOffset = cast(bool)(spriteAttrib[i][3] & 0x40) ? 16 - 2*yDist : 2*yDist;

                        renderPatternLine(read!(ubyte[2])(cast(ushort)( 0x8000+(yIndex*16)+yOffset )), x, lastLine, 
                                          read!ubyte(spriteAttrib[i][3] & 0x10 ? ExtraReg.OBPalette1 : ExtraReg.OBPalette0), 
                                          cast(bool)(spriteAttrib[i][3] & 0x20), true);
                    }
                }

                //Render foreground tile map if bit 5 of the LCDCont register is set
                if (LCDCONT & 0x20) {
                    int posX = read!ubyte(ExtraReg.winXPos)-7;
                    int posY = read!ubyte(ExtraReg.winYPos); 

                    if (posX <= 166 && posY >= 0 && posY <= lastLine && posY <= 143) {
                        tiles = read!(ubyte[32][32])(LCDCONT & 0x40 ? 0x9C00 : 0x9800);
                        foreach (x; 0..32) {
                            //Render each tile in view
                            int tileX = (x*8) - (posX%8);
                            if (tileX > screenW) {
                                break;
                            }
                            if (tileX > -8) {
                                renderPatternLine(getTileLine(tiles[(lastLine-posY)/8][x], cast(ubyte)((lastLine-posY)%8)), 
                                                  posX+(x*8), lastLine, read!ubyte(ExtraReg.BGPalette));
                            }
                        }
                    }
                }
            }
        }

        void renderUpdate() {
            //Update the window
            SDL_Rect target;
            SDL_GetWindowSize(window, &target.w, &target.h);
            if (target.w > target.h) {
                double scale = target.h/144.0;
                target.x = cast(int)(target.w/2 - ((screenW/2)*scale));
                target.w = cast(int)(screenW*scale);
            } else if (target.h > target.w) {
                double scale = target.w/160.0;
                target.y = cast(int)(target.h/2 - ((screenH/2)*scale));
                target.h = cast(int)(screenH*scale);
            }
            SDL_RenderCopy(renderer, SDL_CreateTextureFromSurface(renderer, output), null, &target);
            SDL_RenderPresent(renderer);
            SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0xFF);
            SDL_RenderClear(renderer);
        }

    public:
        void reset(GBType type, bool dbg = false, MMU newMMU = null) {
            deinit;
            init;
            debugMode = dbg;
            this.type = type;
            if (newMMU) {
                mmu = newMMU;
            }
        }

        void update(ulong time) {
            if (debugMode) {
                printDebug;
            }

            //Update the vertical line timer
            ubyte LCDStat = mmu.read!ubyte(ExtraReg.LCDStat);
            if (time-lcdTimer > 1e9/(154*59.7)) with (mmu) {
                lastLine = (lastLine+1)%154;
                write(ExtraReg.curScan, lastLine);
                lcdTimer = time;

                if (read!ubyte(ExtraReg.curScan) == read!ubyte(ExtraReg.scanComp)) {
                    //Set the scanline coincedence flag of the LCDSTAT register and create an interrupt if the flag is set
                    LCDStat |= 0x4;
                    if (LCDStat & 0x40) {
                        write!ubyte(ExtraReg.IFLAGS, read!ubyte(ExtraReg.IFLAGS) | 2);
                    }
                } else {
                    LCDStat &= ~0x4;
                }
                write!ubyte(ExtraReg.LCDStat, LCDStat);

                if (lastLine == 0) {
                    //Reset variables and registers once VBLANK period is over
                    write!ubyte(ExtraReg.curScan, 153);
                    write!ubyte(ExtraReg.LCDStat, LCDStat & 0xFC);
                    vblankSent = false;

                    //Don't display anything if bit 7 of LCDCONT is disabled
                    if (read!ubyte(ExtraReg.LCDCONT) & 0x80) {
                        renderUpdate;

                        //Create an LCDSTAT interrupt for HBLANK if the flag is enabled
                        if (LCDStat & 0x08) {
                            write!ubyte(ExtraReg.IFLAGS, read!ubyte(ExtraReg.IFLAGS) | 2);
                        }
                    }
                } else if (lastLine < 144) {
                    renderLine;
                } else if (lastLine >= 144) {
                    //Create a VBLANK interrupt if bit 7 of LCDCONT is enabled
                    write!ubyte(ExtraReg.LCDStat, (LCDStat & 0xFC) | 1);
                    if ((read!ubyte(ExtraReg.LCDCONT) & 0x80) && !vblankSent) {
                        write!ubyte(ExtraReg.IFLAGS, read!ubyte(ExtraReg.IFLAGS) | 1);
                        vblankSent = true;

                        //Also create an LCDSTAT interrupt if the flag is enabled
                        if (LCDStat & 0x10) {
                            write!ubyte(ExtraReg.IFLAGS, read!ubyte(ExtraReg.IFLAGS) | 2);
                        }
                    }
                }
            }

        }

        ~this() {
            deinit;
        }
}
